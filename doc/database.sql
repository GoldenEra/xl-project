-- 用户表
CREATE TABLE `t_sys_member` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` char(100) NOT NULL,
  `password` char(32) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` char(50) DEFAULT '',
  `group_id` int unsigned DEFAULT '0',
  `sex` varchar(2) DEFAULT '0',
  `is_choose_type` tinyint(1) unsigned DEFAULT '0',
  `open_id` varchar(100) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `reg_time` datetime DEFAULT NULL,
  `last_login_ip` char(15) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `encrypt` varchar(50) DEFAULT NULL,
  `is_lock` tinyint(1) DEFAULT '0',
  `fullname` varchar(50) DEFAULT NULL,
  `qq` varchar(50) DEFAULT NULL,
  `weixin` varchar(50) DEFAULT NULL,
  `is_seller` tinyint(1) DEFAULT '0',
  `is_email_validate` tinyint(1) DEFAULT '0',
  `is_mobile_validate` tinyint(1) DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `province_code` varchar(10) DEFAULT NULL,
  `city_code` varchar(10) DEFAULT NULL,
  `district_code` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `reg_ip` char(15) DEFAULT NULL,
  `is_del` tinyint(1) default 1,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`(15)),
  KEY `email` (`email`),
  KEY `groupID` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8

alter table t_sys_member add column `id_card` varchar(20) not null default '' comment '身份证号' after `email`;
alter table t_sys_member add column `address_id` int not null default 0 comment '地址ID' after `id_card`;
alter table t_sys_member add column `address_detail` varchar(50) not null default '' comment '详细地址' after `address_id`;
alter table t_sys_member add column `is_admin` tinyint not null default 0 comment '区分前台用户和管理员标识' after `password`;


`t_sys_member_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '组ID',
  `role_name` varchar(45) NOT NULL DEFAULT '' COMMENT '组名',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '保留',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `parent_id` smallint(4) DEFAULT '0',
  `arr_childid` varchar(255) DEFAULT NULL,
  `auto_choose` tinyint(1) NOT NULL DEFAULT '1',
  `arr_userid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED

`t_sys_member_role_priv` (
  `priv_id` int(10) unsigned NOT NULL AUTO_INCREMENT comment '主键ID',
  `role_id` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'member_role.id',
  `menu_id` int(11) DEFAULT '0' COMMENT 'module_menue.id',
  `folder` varchar(50) NOT NULL DEFAULT '' COMMENT 'module_menue.folder',
  `controller` varchar(50) NOT NULL DEFAULT '' COMMENT 'module_menue.folder',
  `method` varchar(50) NOT NULL DEFAULT '' COMMENT 'module_menue.folder',
  `data` varchar(50) NOT NULL DEFAULT '' COMMENT '',
  PRIMARY KEY (`priv_id`),
  KEY `role_id` (`role_id`,`folder`,`controller`,`method`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '角色权限表'

drop table `t_sys_member_role_camera`

create table `t_sys_member_role_camera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'member_role.id',
  `camera_id` int(11) DEFAULT '0' COMMENT 'camera.id',
  `address_id` int(11) DEFAULT '0' COMMENT 'camera.address_id',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `is_del` tinyint(1) default 1,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`,`camera_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '角色的摄像头权限表';

alter table t_sys_message add column `level` int not null default 0 comment '向上报的次数' after `type`;

CREATE TABLE `t_sys_module_menu` (
  `menu_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` char(40) NOT NULL DEFAULT '',
  `parent_id` smallint(6) NOT NULL DEFAULT '0',
  `list_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `is_display` tinyint(1) NOT NULL DEFAULT '1',
  `controller` varchar(50) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `flag_id` varchar(50) NOT NULL DEFAULT '0',
  `is_side_menu` tinyint(1) DEFAULT '0',
  `is_system` tinyint(1) DEFAULT '0',
  `is_works` tinyint(1) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `css_icon` varchar(50) DEFAULT NULL,
  `arr_parentid` varchar(250) DEFAULT NULL,
  `arr_childid` varchar(250) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT '0',
  `show_where` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`menu_id`) USING BTREE,
  KEY `list_order` (`list_order`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT '栏目管理表'


CREATE TABLE `t_sys_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE `t_sys_times` (
  `times_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `login_ip` char(15) DEFAULT NULL COMMENT 'ip',
  `login_time` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `failure_times` int(10) unsigned DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`times_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8


-- 地址信息
CREATE TABLE `t_sys_address` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `name` varchar(256) NOT NULL default '' COMMENT '名字',
  `level` int(10) NOT NULL DEFAULT '1' COMMENT '1:省 2:市 3:区/县 4:街道/镇',
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '父级id',
  `parent_ids` char(20) NOT NULL DEFAULT '0' COMMENT '所有父级别id',
  `zipcode` varchar(100) NOT NULL DEFAULT '' COMMENT '邮编',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除,默认不删除',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地址管理表';


-- 摄像头信息
CREATE TABLE `t_sys_camera` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '摄像头id',
  `camera_name` varchar(50) not null default '' COMMENT '名字',
  `address_id` int not null default 0 COMMENT '地址ID',
  `desc` char(255) not null default '' COMMENT '描述',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除,默认不删除',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='摄像头管理表';

alter table t_sys_camera add column `custom_id` varchar(30) not null default '' comment 'DAHUA.channel.id, HAIKANG.c_device_index_code' after `camera_name`;



-- 消息
CREATE TABLE `t_sys_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repoter_id` int unsigned not null default 0 comment '报警人',
  `repoter_address_id` int unsigned not null default 0 comment '报警人地址',
  `receive_user_id` int unsigned not null default 0 comment '处理人ID',
  `type`  varchar(30) not null default 'DIRECT' comment '报警类别，110,119,120等',
  `content` text comment '报警内容',
  `longitude` varchar(50) default '' comment '经度',
  `latitude` varchar(50) default '' comment '纬度',
  `address_detail` varchar(50) default '' comment '详细地址',
  `address` varchar(255) default '' comment '报警内容',
  `image` varchar(512) not null default '' comment '报警图片地址',
  `video` varchar(512) not null default '' comment '报警视频地址',
  `thumb` varchar(512) not null default '' comment '报警视频缩略图地址',
  `status` varchar(20) not null default 'CREATED' comment 'CREATED: 未处理 PROCESS:处理中 FINISH:处理完成',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报警信息记录表';


CREATE TABLE `t_sys_message_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int unsigned not null default 0 comment 'message.id',
  `status` varchar(20) not null default 'CREATED' comment 'CREATED: 未处理 FINISH:处理完成',
  `staff_id` int not null default 0 comment '处理人的ID',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报警信息日志';



