alter table t_sys_material add column `content` longtext  comment '文档内容' after `type`;
alter table t_sys_material add column `thumb` varchar(255) not null default ''  comment '视频缩略图' after `path`;


alter table t_sys_member add column `longitude` varchar(50) DEFAULT '' COMMENT '经度' after `mobile`;
alter table t_sys_member add column `latitude` varchar(50) DEFAULT '' COMMENT '纬度' after `longitude`;