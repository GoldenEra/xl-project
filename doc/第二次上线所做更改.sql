alter table t_sys_camera add column provider char(20) not null 
default 'DAHUA' comment '摄像头提供商 大华:DAHUA, 海康:HAIKANG 宇视:YUSHI' after `camera_name`


create table `t_sys_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(155) DEFAULT '' COMMENT '资料名字',
  `material_type_id` int not null default 0 comment '类型ID, material_type.id',
  `desc` text  COMMENT '材料描述',
  `type` varchar(30) not null default 'image' comment '文件类型，主要有 image:图片, video:视频 text:文本',
  `suffix` varchar(30) not null default 'jpg' comment '文件后缀',
  `path` varchar(255) DEFAULT '' COMMENT '材料路径',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `material_type_id` (`material_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '报警材料管理';


create table `t_sys_material_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(155) DEFAULT '' COMMENT '标签名',
  `level` int(10) NOT NULL DEFAULT '1' COMMENT '所处级别',
  `icon` varchar(255) DEFAULT '' COMMENT '分类图标路径',
  `desc` text  COMMENT '分类描述',
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '父级id',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '上传材料分类表';