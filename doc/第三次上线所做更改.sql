alter table t_sys_camera add column `longitude` varchar(50) default '' comment '经度' after `address_id`;
  
alter table t_sys_camera add column `latitude` varchar(50) default '' comment '纬度' after `longitude`;

alter table t_sys_camera add column `c_device_code` varchar(100) default '' comment 'DAHUA.device.id 设备ID' after `status`;
alter table t_sys_camera add column `c_device_status` varchar(100) default '' comment 'DAHUA.c_device_status 设备索引' after `c_device_code`;


alter table t_sys_camera add column `c_index_code` varchar(100) default '' comment 'HAIKANG.c_index_code 监控索引' after `latitude`;

alter table t_sys_camera add column `status` smallint default 1 comment 'DAHUA.status' after `latitude`;

alter table t_sys_camera add column `i_channel_no` int not null default 0 after `HAIKANG.i_channel_no c_index_code`;

alter table t_sys_camera add column `i_ptz_type` smallint default 0 comment 'HAIKANG.i_ptz_type PTZ类型 （1表示可转动 变焦） 2（仅变焦） 3（仅转动） 4（无转动 无变焦）' after `c_index_code`;

alter table t_sys_camera add column `i_status` smallint default -1 comment 'HAIKANG.i_status 0表示正常' after `i_ptz_type`;

alter table t_sys_camera add column `i_is_online` smallint default -1 comment 'HAIKANG.i_is_online 1表示在线' after `i_status`;

alter table t_sys_camera add column `vtm_ip` varchar(30) default '' comment 'HAIKANG.vtm.ip' after `i_is_online`;

alter table t_sys_camera add column `vtm_port` varchar(10) default '' comment 'HAIKANG.vtm.port' after `vtm_ip`;

alter table t_sys_camera add column `vtm_rtsp_port` varchar(10) default '' comment 'HAIKANG.vtm.rtsp_port' after `vtm_port`;

alter table t_sys_camera add column `vag_port` varchar(10) default '' comment 'HAIKANG.vag.pot' after `vtm_rtsp_port`;

alter table t_sys_member modify column `id_card` varchar(255) default '' comment '身份证号';
