CREATE INDEX custom_id_index ON t_sys_camera (custom_id);

alter table t_sys_message add column `level` int not null default 0 comment '向上报的次数' after `type`;

alter table t_sys_camera add column `camera_type` int not null default 0 comment '摄像头类型' after `provider`;
