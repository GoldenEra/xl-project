流程梳理：

1. 用户登陆注册（手机号唯一标识）
2. 权限功能
  1. 用户---用户组---用户组权限---权限
3. 手动上报功能
4. 自动报警
5. 如果第四步未处理直接上报上一级

数据库设计：
用户（管理员）表
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT, 
  `phone` varchar(15) not null default '' comment '电话号码',
  `gender` smallint not null default 1 comment '性别，0：女，1:男',
  `locate_code` smallint not null default 0 comment '省市区街道四级地址',
  `ip` bigint NOT NULL DEFAULT '0', 
  `is_del` enum('Y','N') NOT NULL DEFAULT 'N', 
  `created` int NOT NULL DEFAULT '0' comment '创建时间', 
  `modified` int NOT NULL DEFAULT '0' comment '修改时间', 
  PRIMARY KEY(`id`),
  KEY `shop_id` (`shop_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='不同维修故障所支持的维修方式管理表';

管理员表：

角色表：

权限表：


用户-角色表：

摄像头表：
camera:

地址表：

消息表：

消息处理状态表：

window.open("/adminpanel/roleCamera/setting/"+group_id+'/'+address_id);







