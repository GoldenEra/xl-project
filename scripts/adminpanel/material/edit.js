define(function (require) {
    var $ = require('jquery');
    var aci = require('aci');
    require('bootstrap');
    require('jquery-ui-dialog-extend');
    require('bootstrapValidator');
    require('message');
    
    require('ueditor');
    require('ueditor-config');
    require('ueditor-parse');

    $(document).ready(function(){
        var ue = new UE.getEditor('ueditor-container', {
        
        UEDITOR_HOME_URL: '/scripts/lib/ueditor/',
        serverUrl: '/scripts/lib/ueditor/php/controller.php',
        initialFrameWidth: 700,
        initialFrameHeight: 200,
        toolbars: [
                [ "fontsize","Bold", "Italic", "forecolor","insertorderedlist","insertunorderedlist","blockquote","simpleupload", "insertimage", "Link", "justifyleft", "justifycenter", "justifyright", "preview","source","fullscreen"]
        ],
        // fontsize: [14,16,18],
        wordCount:true,
        elementPathEnabled: false,
        autoHeightEnabled: true,
        autoFloatEnabled: true,
        topOffset:60,
        enableContextMenu: false,
        customDomain: true,
        pasteplain: true,
        allowDivTransToP: true,
        maxListLevel : '-1', //[默认值：3] // 限制可以 tab 的级数， 设置 - 1 为不限制
        autoTransWordToList: true, // 禁止 word 中粘贴进来的列表自动变成列表标签


        whitList: {
            a:      ['target', 'href', 'title', 'class', 'style'],
            abbr:   ['title', 'class', 'style'],
            address: ['class', 'style'],
            area:   ['shape', 'coords', 'href', 'alt'],
            article: [],
            aside:  [],
            audio:  ['autoplay', 'controls', 'loop', 'preload', 'src', 'class', 'style'],
            b:      ['class', 'style'],
            bdi:    ['dir'],
            bdo:    ['dir'],
            big:    [],
            blockquote: ['cite', 'class', 'style'],
            br:     [],
            caption: ['class', 'style'],
            center: [],
            cite:   [],
            code:   ['class', 'style'],
            col:    ['align', 'valign', 'span', 'width', 'class', 'style'],
            colgroup: ['align', 'valign', 'span', 'width', 'class', 'style'],
            dd:     ['class', 'style'],
            del:    ['datetime'],
            details: ['open'],
            div:    ['class', 'style'],
            dl:     ['class', 'style'],
            dt:     ['class', 'style'],
            em:     ['class', 'style'],
            font:   ['color', 'size', 'face'],
            footer: [],
            h1:     ['class', 'style'],
            h2:     ['class', 'style'],
            h3:     ['class', 'style'],
            h4:     ['class', 'style'],
            h5:     ['class', 'style'],
            h6:     ['class', 'style'],
            header: [],
            hr:     [],
            i:      ['class', 'style'],
            img:    ['src', 'alt', 'title', 'width', 'height', 'id', '_src', 'loadingclass', 'class', 'data-latex'],
            ins:    ['datetime'],
            li:     ['class', 'style'],
            mark:   [],
            nav:    [],
            ol:     ['class', 'style'],
            p:      ['class', 'style'],
            pre:    ['class', 'style'],
            s:      [],
            section:['class', 'style'],
            small:  [],
            span:   ['class', 'style'],
            sub:    ['class', 'style'],
            sup:    ['class', 'style'],
            strong: ['class', 'style'],
            table:  ['width', 'border', 'align', 'valign', 'class', 'style'],
            tbody:  ['align', 'valign', 'class', 'style'],
            td:     ['width', 'rowspan', 'colspan', 'align', 'valign', 'class', 'style'],
            tfoot:  ['align', 'valign', 'class', 'style'],
            th:     ['width', 'rowspan', 'colspan', 'align', 'valign', 'class', 'style'],
            thead:  ['align', 'valign', 'class', 'style'],
            tr:     ['rowspan', 'align', 'valign', 'class', 'style'],
            tt:     [],
            u:      [],
            ul:     ['class', 'style'],
            video:  ['autoplay', 'controls', 'loop', 'preload', 'src', 'height', 'width', 'class', 'style']
        },

        filterTxtRules : function(){
            return {
                    //直接删除及其字节点内容
                    'p': {$:{}},
                    'img': {$:{'src':1,'width': 1, 'height':1}},
                    'ol':{$: {}},
                    'li':{$: {}},
                    'ul': {$: {}},
                    'table': {$: {}},
                    'td': {$: {}},
                    'tr': {$: {}},
                    'strong': {$: {}},
                    'h1': {$: {}},
                    'h2': {$: {}},
                    'h3': {$: {}},
                    'a': function (node) {
                        if(!node.firstChild()){
                            node.parentNode.removeChild(node);
                            return;
                        }
                    }
                }
            }()
        });

        type = $('.type option:selected')[0].value;
        show_or_hidden(type);
    });

    uParse('.content', {
        rootPath: '/scripts/lib/ueditor/',
    })

    var validator_config = {
        message: '输入框不能为空',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            material_name: {
                message: '资料名不能为空',
                validators: {
                    notEmpty: {
                        message: '资料名不能为空'
                    }
                }
            }
        }
    };

    $('.type select').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var type = this.value;
        show_or_hidden(type);
        // console.log(type);
    });

    function show_or_hidden(type) {
        if (type == 'text') {
            $('.ueditor-content').show();
            $('.material_name').show();
            $('.material_file').hide();
        }else{
            $('.ueditor-content').hide();
            $('.material_name').hide();
            $('.material_file').show();
        }
    }

    $('#dosubmit').click(function () {

        var depth = $('#type').find(":selected").attr("value");

        var depth = $('#type_id').find(":selected").attr("depth");
        if (depth != 2) {
            alert('只能选择二级分类');
            return false;
        }

        type = $('.type option:selected')[0].value;
        if (type == 'text') {

        }else if(type == 'other'){
            var uploadField = document.getElementById("material_file_1");
            if( uploadField.files.length == 0 ){
                alert('请先选择文件');
                return false;
            }

            var uploadFiles = uploadField.files;
            // console.log(uploadFiles);

            if (uploadFiles.length > 10) {
                alert('一次最多只能上传10个文件');
                return false;
            }

            var fileSize = 0;
            for (var i = 0; i < uploadFiles.length; i++) {
                var size = uploadFiles[i].size / 1024 / 1024;
                // console.log(size);

                if (size > 1024) {
                    alert('每个文件不能大于1G');
                    return false;
                }
                fileSize += size;
            }
            // console.log(fileSize);

            if (fileSize > 2048) {
                alert('文件总大小不能超过2G');
                return false;
            }
            
        }else{
            alert('类型选择错误');
            return false;
        }
        $('.over_layer').css('display', 'block');
        $('.layer_dialog').css('display', 'block');
        
        // 添加遮罩层
        // 
        // document.getElementById("show").style.display ="block";
    });
/**
    $('#validateform').bootstrapValidator(validator_config).on('success.form.bv', function(e) {
        e.preventDefault();

        $("#dosubmit").attr("disabled","disabled");


        $.scojs_message('请稍候...', $.scojs_message.TYPE_WAIT);
        $.ajax({
            type: "POST",
            url: SITE_URL+folder_name+"/material/add/",
            data:  $("#validateform").serialize(),

            success:function(response){
                console.log(response);
                var dataObj=jQuery.parseJSON(response);
                if(dataObj.status)
                {
                    $.scojs_message('操作成功,3秒后将返回列表页...', $.scojs_message.TYPE_OK);
                    aci.GoUrl(SITE_URL+folder_name+'/material/index/',1);
                }else
                {
                    $.scojs_message(dataObj.tips, $.scojs_message.TYPE_ERROR);
                    $("#dosubmit").removeAttr("disabled");
                }
            },
            error: function (request, status, error) {
                $.scojs_message(request.responseText, $.scojs_message.TYPE_ERROR);
                $("#dosubmit").removeAttr("disabled");
            }
        });

    }).on('error.form.bv',function(e){ $.scojs_message('带*号不能为空', $.scojs_message.TYPE_ERROR);$("#dosubmit").removeAttr("disabled");});
**/
});