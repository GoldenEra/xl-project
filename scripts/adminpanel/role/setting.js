define(function (require) {
    var $ = require('jquery');
    require('bootstrap');

    $('input').change(function () {
    	var is_checked = this.checked;
    	var is_parent = $(this).data('is_parent');

    	var parent_ids = $(this).data('arr_parentid').toString().split(',');
    	var child_ids = $(this).data('arr_childid').toString().split(',');

    	// 1. 选中父母
    	//   1.1 不做任何操作
    	// 2. 取消选中父母
    	// 	2.1 取消所有选中孩子
    	// 3. 选中孩子
    	// 	3.1 选中所有父母
    	// 4. 取消选中孩子
    	//   4.1 不做任何操作
    	
    	// 父母取消选中
    	if (!is_checked && is_parent) {
    		changeCheck(child_ids, false);
    	}

    	// 选中所有孩子
    	if (is_checked) {
    		changeCheck(parent_ids, true);
    	}

    });

    function changeCheck(ids, bool_checked) {

    	for (var i = 0; i < ids.length; i++) {
    		var defined = $('[data-id="'+ids[i]+'"]')[0];
    		if (defined) {
    			defined.checked = bool_checked;
    		}
    	}

    }


});