define(function (require) {
	var $ = require('jquery');
	var aci = require('aci');
	
	require('bootstrap');
	require('jstree');
	require('treeview');

	$('#container').jstree();

	$("#reverseBtn").click(function(){
		aci.ReverseChecked('pid[]');
	});

	$("#setMenuBtn").click(function(){
		var _arr = aci.GetCheckboxValue('pid[]');
		if(_arr.length==0)
		{
			alert("请先勾选明细");
			return false;
		}

		if(confirm("确定要反设置左侧菜单？"))
		{
			$("#formlist").attr("action",SITE_URL+folder_name+"/"+controller_name+"/set_menu/");
			$("#formlist").submit();
		}
	});

	$("#deleteBtn").click(function(){
		var _arr = aci.GetCheckboxValue('pid[]');
		if(_arr.length==0)
		{
			alert("请先勾选明细");
			return false;
		}

		if(confirm("确定要删除所选地址？"))
		{
			$("#formlist").attr("action",SITE_URL+folder_name+"/"+controller_name+"/delete/");
			$("#formlist").submit();
		}
	});


	// $(document).on( "click", ".jstree-anchor", function() {
		
	//   	var address_id = $(this).parent().data('id');
	//   	address_id = stripscript(address_id.toString());
	// 	window.open("edit/"+address_id);
	// });

	function stripscript(s) 
	{ 
		var pattern = new RegExp("[%--`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
		var rs = ""; 
		for (var i = 0; i < s.length; i++) { 
		 rs = rs+s.substr(i, 1).replace(pattern, ''); 
		}
		return rs;
	}

});
