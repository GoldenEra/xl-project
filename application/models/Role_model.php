<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*/
class Role_model extends Member_role_model {
    public function __construct() {
        parent::__construct();
    }

    function group_dropdown_datasource($where='',$limit = '', $order = '', $group = '', $key=''){
    	$datalist = $this->select($where,'role_id,role_name',$limit,$order,$group,$key);
        return $datalist;
    }

    /**
     * 用户组弹窗
     * @return array
     */
    function group_window_datasource($where='',$limit = '', $order = '', $group = '', $key=''){
    	
    	$datalist = $this->select($where,'role_id,role_name',$limit,$order,$group,$key);
        return $datalist;
    } 

    /**
     * 用户组下拉值
     * @return array
     */
    function group_dropdown_value($id){
        
        $datainfo = $this->get_one(array('role_id'=>$id));
        return isset($datainfo['role_name'])?$datainfo['role_name']:'-';
    }

    public function isRoleExist($id = 0)
    {
        if (empty($is) || !is_int($id)) {
            
            return false;
        }

        return $this->get_one(['role_id' => $id]);
    }

    public function getAllGroups($group_id = 0) {

        $all_groups = $this->Member_role_model->select();

        $options[] =array('caption'=>'==请选择==','value'=>'');
        foreach($all_groups as $k=>$v)
        $options[] =array('caption'=>$v['role_name'],'value'=>$v['role_id']);
        
        $_html="";
        foreach($options as $k=>$v)
        {
            if($group_id == $v['value'])
                $_html .="<option value=\"{$v['value']}\" selected=\"selected\">{$v['caption']}</option>";
            else
                $_html .="<option value=\"{$v['value']}\" >{$v['caption']}</option>";
        }
        
        return $_html;
    }

}