<?php 
class Material_model extends Base_Model {
    public function __construct() {

        // 表前缀
        $this->table_name = 'material';

        parent::__construct();
        $this->load->model(['Material_type_model']);
    }

    /**
     * 获取所有材料列表
     * 
     * @return [type]      [description]
     */
    public function getMaterialList()
    {
        return $this->select('', '*');
    }

   
    public function getByTypeID($type_id)
    {
        return $this->select('material_type_id='.$type_id, '*');
    }

    /**
     * 增加分类列表
     * @param array $params [description]
     */
    public function add($params = [])
    {
        if (empty($params)) {
            
            return json_encode(array('status' => -1,'tips' => "参数为空"));
        }

        $isLevelTwo = $this->Material_type_model->isLevelTwo($params['material_type_id']);
        if ($isLevelTwo['status'] !== 200) {
            
            return array('status' => -1,'tips'=>"所选材料分级不正确");
        }
        
        $insert_id = $this->insert($params);

        if (empty($insert_id)) {
            
            return array('status'=>-1,'tips'=>"新增材料失败");
        }

        return array('status' => 200,'tips'=>"新增材料成功", 'data' => $insert_id);
    }

    public function formateMaterialTree($material_list = [], $material_id = 0)
    {

        $str  = "<option depth='\$depth' value='\$id' \$selected>\$spacer \$name </option>";
        
        if (!empty($id)) {
            foreach($material_list as &$material) {

                $material['selected'] = $material['id'] == $id ? 'selected' : '';
            }
        }

        $tree = $this->tree;
        $tree->init($material_list);

        return $tree->get_tree(0, $str);
    }


    /**
     * 增加地址列表
     * @param array $update_field [description]
     */
    public function update_material($id = 0, $update_field = [])
    {
        if (empty($update_field)) {
            
            return array('status' => -1,'tips' => "更新记录为空");
        }

        $current = $this->get_one('id='.$id);
        if (empty($current)) {
            
            return array('status' => -1,'tips' => "未找到记录");
        }

        $isLevelTwo = $this->Material_type_model->isLevelTwo($update_field['material_type_id']);
        if ($isLevelTwo['status'] !== 200) {
            
            return array('status' => -1,'tips' => "所选分类不正确");
        }

        $result = $this->update($update_field, 'id='.$id);
        if ($result) {
            
            return array('status' => 200,'tips' => "更新成功");
        }

        return array('status' => -1,'tips' => "更新失败");

    }


    public function delete_material($material_ids = [])
    {
        if (empty($material_ids)) {

            return array('status'=>false,'tips'=>"未选中任何记录");
        }

        $ids = implode(',', $material_ids);
        
        $this->delete('id in ('.$ids.')');

        return array('status'=>true,'tips'=>"删除成功");

    }


    

    public function isNameExist($name = '')
    {
        return $this->get_one("name='$name'");
    }

    /**
     * 获取视频截图
     * @param  string $video_path [description]
     * @return [type]             [description]
     */
    public function getVideoThumb($video_path = '')
    {
        if (empty($video_path)) {
            
            return array('status' => -1,'tips' => "视频名称为空");
        }
        if (!file_exists($video_path)) {
            
            return array('status' => -1,'tips' => "视频不存在");
        }

        $video_detail = pathinfo($video_path);
        if (!$video_detail) {

            return array('status' => -1,'tips' => "视频路径解析错误");
        }
        // var_dump($video_detail);exit;

        if (empty($video_detail['filename'])) {
            
            return array('status' => -1,'tips' => "视频名字错误");
        }
        $video_name = $video_detail['filename'];

        if (empty($video_detail['extension'])) {
            
            return array('status' => -1,'tips' => "视频名字错误");
        }
        $video_suffix = $video_detail['extension'];

        $ffmpeg = '/usr/bin/ffmpeg';
        //where to save the image
        $image_name = $video_name.'thumb.jpg';
        $image = UPLOAD_PATH.'image'.DIRECTORY_SEPARATOR.$image_name;
        $save_path = 'image'.DIRECTORY_SEPARATOR.$image_name;

        //time to take screenshot at
        $interval = 2;

        //screenshot size
        $size = '320x240';

        //ffmpeg command
        $cmd = "$ffmpeg -i $video_path -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $size $image 2>&1";  

        $result = exec($cmd);
        if (empty($result)) {
            error_log(date('Y-m-d H:i:s', time())." 执行exec命令失败", 3, LOG_PATH.'generate_thumb.log');
            return array('status' => -1, 'data' => '', 'tips' => "生成截图失败");

        }   

        return array('status' => 200, 'data' => $save_path, 'tips' => "生成截图成功");

    }

    /*
    *    默认信息
    */
    function default_info() {

        return array(
            'name' => "",
            'level' => "",
             'type' => "image",
            'desc' => "",
            'parent_id' => 0,
        );
    }

}
