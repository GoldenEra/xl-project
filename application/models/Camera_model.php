<?php 
class Camera_model extends Base_Model {

    private $_first_token = '';
    private $_second_token = '';

    private $_loginWebLocation = 'http://'.HAIKANG_SERVER_IP.':'.HAIKANG_SERVER_PORT.'/cms/services/IAuthService?wsdl';
    private $_resourceLocation = 'http://'.HAIKANG_SERVER_IP.':'.HAIKANG_SERVER_PORT.'/cms/services/IAuthService?wsdl';
    private $_allResource = 'http://'.HAIKANG_SERVER_IP.':'.HAIKANG_SERVER_PORT.'/cms/services/ICuService?wsdl';

    public function __construct() {

        $this->table_name = 'camera';
        parent::__construct();
        $this->load->model(array('Address_model'));
        $this->load->model(array('User_model'));
        $this->load->model(array('Member_model'));
        $this->load->model(array('Role_camera_model'));

    }

    public function add($camera_name = '', $custom_id = '', $address_id = 1, $provider = 'DAHUA', $camera_type = '', $longitude = '', $latitude = '')
    {
        if (empty($camera_name)) {
            
            return array('status'=> -1,'tips'=>"参数为空", 'data' => []);

        }

        // 验证地址
        if(!$this->Address_model->get_one(['id' => $address_id])) {

            return ['status' => -1, 'tips' => '地址不存在', 'data' => ''];
        }

        $params = ['camera_name' => $camera_name, 'custom_id' => $custom_id, 
                    'address_id' => $address_id, 'provider' => $provider, 
                    'longitude' => $longitude, 'latitude' => $latitude];
        if (!empty($camera_type)) {
            
            $params['camera_type'] = $camera_type;
        }
        
        $insert_id = $this->insert($params);

        if (empty($insert_id)) {
            
            return array('status'=> -1,'tips'=>"新增摄像头失败");
        }
        $data = $this->get_one(['id' => $insert_id]);

        return array('status' => 200,'tips'=>"新增摄像头成功", 'data' => $data);
    }

    // public function updateCamera($camera_id = 0, $camera_name = '', $custom_id = '', $address_id = 0)
    // {
    //     if (!empty($camera_id)) {

    //         return array('status' => -1,'tips'=>"摄像头ID为空", 'data' => []);
    //     }
    //     $update_field = [];
    //     if (!empty($camera_name)) {
            
    //         $update_field['camera_name'] = $camera_name;
    //     }

    //     if (!empty($custom_id)) {
            
    //         $update_field['custom_id'] = $custom_id;
    //     }

    //     if (!empty($address_id)) {
            
    //         $update_field['address_id'] = $address_id;
    //     }
        
    //     return $this->update($update_field, 'id='.$camera_id);
    // }

    /**
     * 获取当前用户所有用户的摄像头信息
     * 
     * @param  integer $role_id    [description]
     * @param  integer $page       [description]
     * @param  integer $pagesize   [description]
     * @param  boolean $front_show  是否用做前端显示，
     * @return [type]              [description]
     */
    public function showList($role_id = 0, $page = 1, $pagesize = 20, $front_show = 0)
    {
        if ($front_show) {
            $join_stytle = 'join';
           
        } else {
            $join_stytle = 'left join';

        }

        $total = $this->getRoleCameraCount($role_id, $front_show);

        $page = max(intval($page), 1);
        $offset = $pagesize * ($page - 1);
        
        // 如果偏移量超过摄像头总个数
        if($offset > $total)
        {
            $page = round($total / $pagesize);
            $offset = max($pagesize * ($page-1),0);
        }


        $sql = "select 
                    c.*,
                    c.id as camera_id,
                    role_id
                    from t_sys_camera c 
                    ". $join_stytle ." 
                    t_sys_member_role_camera rc on rc.camera_id=c.id 
                    and rc.role_id=".$role_id." limit " .$pagesize. " offset ".$offset;


        return $this->select_sql($sql);
    }


    /**
     * 获取前台用户摄像头列表信息
     * @param  integer $user_id  [description]
     * @param  integer $page     [description]
     * @param  integer $pagesize [description]
     * @return [type]            [description]
     */
    public function cameraList($user_id = 0, $page = 1, $pagesize = 20)
    {
        if (empty($user_id)) {
            
            return ['status' => -1, 'tips' => '用户ID为空', 'data' => ''];
        }

        $user_info = $this->Member_model->get_one(array('user_id'=>$user_id));
        if (!$user_info) {
            
            return ['status' => -1, 'tips' => '用户不存在', 'data' => ''];
        }

        $role_id = $user_info['group_id'];

        $camera_list = $this->showList($role_id, $page, $pagesize, 1);
        if (!$camera_list) {
            
            return ['status' => -1, 'tips' => '未找到摄像头信息', 'data' => ''];
        }

        $data['list'] = $camera_list;
        $data['total'] = $this->getRoleCameraCount($role_id, 1);
            
        return ['status' => 200, 'tips' => '找到摄像头信息', 'data' => $data];
    }

    private function getRoleCameraCount($role_id = 0, $front_show = 0)
    {
        if ($front_show) {
            $join_stytle = 'join';
           
        } else {
            $join_stytle = 'left join';

        }

        $total = "select 
                    count(*) as num
                    from t_sys_camera c 
                    ". $join_stytle ." 
                    t_sys_member_role_camera rc on rc.camera_id=c.id 
                    and rc.role_id=".$role_id;

        $allCount = $this->select_sql($total);

        if (!$allCount || empty($allCount[0]['num'])) {
           
           return 0;
        }

        return $allCount[0]['num'];
    }

    /**
     * 获取当前组的分配的摄像头列表
     * 
     * @param  integer $page     [description]
     * @param  integer $count    [description]
     * @param  integer $group_id [description]
     * @return [type]            [description]
     */
    public function getCameraList($page = 1, $count = 200, $group_id = 0)
    {

        $sql = "select 
                    c.*
                    from t_sys_camera c 
                    join t_sys_member_role_camera rc on rc.camera_id=c.id 
                    and rc.role_id=".$group_id;

        return $this->select_sql($sql);
    }

    public function updateCamera($id = 0, $params = [])
    {
        $isCameraExits = $this->get_one('id='.$id);
        if (!$this->get_one('id='.$id)) {
            
            return ['status' => 'false', 'tips' => '该摄像头不存在'];
        }

        $reuslt =  $this->update($params, 'id='.$id);
        if ($reuslt) {
            
            return ['status' => 200, 'tips' => '更新成功', 'data' => $reuslt];
        }
        
        return ['status' => -1, 'tips' => '更新失败', 'data' => []];
    }

    public function delete_camera($ids = [])
    {
        is_array($ids) or $ids = [$ids];

        $where = implode(',', $ids);

        // 删除绑定的摄像头
        $this->Role_camera_model->delete('camera_id in ('. $where . ')');
        
        return $this->delete('id in ('. $where . ')');        
    }

    /**
     * 导入摄像头信息
     * @param  string $file_name [description]
     * @param  string $type      [description]
     * @return [type]            [description]
     */
    public function importDaHuaCameras()
    {
        // $file_path = UPLOAD_PATH.'cameraInfo'.DIRECTORY_SEPARATOR.$file_name;
        $file_path = '/home/dahuacamerainfo.xml';
        if (file_exists($file_path)) {
            $info = simplexml_load_file($file_path);
        } else {
            exit('Failed to open file');
        }
        // $arr = json_decode(json_encode((array)$info), 1);
        // 由于这里的操作设计大量的查找，插入，故将程序执行时间设置长一点（5min）
        ini_set('max_execution_time', 300);
        $total_count = $failed_count = 0;
        foreach ($info->Department->Department as $level_one_departments) {

            $level_one_name = (string)$level_one_departments['name'];
            $level_one_address_params = $this->buildAddressParams($level_one_name);
            // var_dump($level_one_address_params);exit;
            list($level_one_address, $level_one_address_id) = $level_one_address_params;

            foreach ($level_one_departments->Department as $level_two_departments) {
                
                $level_two_name = (string)$level_two_departments['name'];

                $level_two_address_params = $this->buildAddressParams($level_two_name, $level_one_address_id);
                list($level_two_address, $level_two_address_id) = $level_two_address_params;

                foreach ($level_two_departments->Department as $level_three_departments) {
                    
                    $level_three_name = (string)$level_three_departments['name'];
                
                    $level_three_address_params = $this->buildAddressParams($level_three_name, $level_two_address_id);
                    
                    list($level_three_address, $level_three_address_id) = $level_three_address_params;
                    
                    foreach ($level_three_departments->Department as $level_four_departments) {
                        
                        $level_four_name = (string)$level_four_departments['name'];
                
                        $level_four_address_params = $this->buildAddressParams($level_four_name, $level_three_address_id);
                    
                        list($level_four_address, $level_four_address_id) = $level_four_address_params;

                        $i = 0;
                        foreach ($level_four_departments->Channel as $channel) {
                                // 数据库已经存在的，不再添加,在device列表中不存在的，也不添加
                                $channel_id = (string)$channel['id'];
                                $found = $info->xpath('//Device//Channel[@id="'.$channel_id.'"]');

                                if (empty($found)) {
                                    $failed_count++;
                                    continue;
                                }
                                $deviceInfo = $found[0]->xpath("parent::*")[0]->xpath("parent::*");

                                if (empty($deviceInfo)) {
                                    $failed_count++;
                                    continue;
                                }
                                
                                $add_params = array(
                                        'camera_name'       => $level_four_name.'摄像头'.$i,
                                        'camera_type'       => (int)$found[0]['cameraType'],
                                        'provider'          => 'DAHUA',
                                        'custom_id'         => $channel_id,
                                        'address_id'        => $level_four_address_id,
                                        'longitude'         => (string)$found[0]['longitude'],
                                        'latitude'          => (string)$found[0]['latitude'],
                                        'status'            => (int)$found[0]['status'],
                                        'c_device_code'     => (string)$deviceInfo[0]['id'],
                                        'c_device_status'   => (int)$deviceInfo[0]['status'],
                                );

                                $isCameraExits = $this->get_one(['custom_id' => $channel_id]);

                                if ($isCameraExits) {
                                    $camera_id = $isCameraExits['id'];
                                    $result = $this->update($add_params, 'id='.$camera_id);
                                }else{

                                    $result = $this->insert($add_params);
                                }

                                $total_count++;

                                if (empty($result)) {
                                    
                                    $failed_count++;
                                }
                                $i++;
                              
                        }
                    }
                    
                }
            }
        }

        $data = ['total_count' => $total_count, 'failed_count' => $failed_count];

        return ['status' => 200, 'tips' => "添加成功 $total_count 个, 失败 $failed_count 个", 'data' => $data];
    }

    public function isChannelExist($channel_id = 0)
    {
        return $this->get_one(['custom_id' => $channel_id]);
    }

    public function cameraMap()
    {
        $count = MAX_PAGE_SIZE;
        $group_id = $this->group_id;
        $where = 'role_id='.$group_id;
        $orderby = 'created';

        $data_list = $this->Role_camera_model->listinfo($where, '*',$orderby , 1, $count, '',$count);
        $provider_arr = ['DAHUA' => '大华', 'YUSHI' => '宇视', 'HAIKANG' => '海康'];
        foreach ($data_list as &$camera) {
            
            $camera_info = $this->Camera_model->get_one(['id' => $camera['camera_id']]);
			if(empty($camera_info)) {

				$camera = [];
				continue;
			}
            $provider = empty($camera_info['provider']) ? 'DAHUA' : $camera_info['provider'];
            $camera['provider_cn'] = isset($provider_arr[$provider])
                                     ? $provider_arr[$provider]
                                     : '未知类型';
            $camera = array_merge($camera, $camera_info);
            $camera['address_detail'] = $this->Address_model->getAddressDetailByID($camera['address_id']);

            // 下面的数据是为了地图显示的
            $camera['center'] = [floatval($camera['longitude']), floatval($camera['latitude'])];
            $camera['name'] = $camera['camera_name'];
            $camera['show_type'] = 'camera';
        }
		$data_list = array_filter($data_list);

        return ['status' => 200, 'tips' => '摄像头列表获取成功', 'data' => $data_list];
    }


    /**
     * 更新大华摄像头状态接口字段
     * @param  string $custom_id [description]
     * @param  string $status    [description]
     * @return [type]            [description]
     */
    public function updateStatus($custom_id = '', $status = '')
    {
        $camera_info = $this->get_one(array('custom_id' => $custom_id));
        if (!$camera_info) {

            return ['status' => -1, 'tips' => '摄像头信息不存在', 'data' => []];
        }

        $reuslt =  $this->update(array('status' => $status), "custom_id='$custom_id'");

        if ($reuslt) {
            
            return ['status' => 200, 'tips' => '更新成功', 'data' => $reuslt];
        }
        
        return ['status' => -1, 'tips' => '更新失败', 'data' => []];

    }


    public function updateDeviceStatus($c_device_code = '', $c_device_status = '')
    {
        $camera_info = $this->get_one(array('c_device_code' => $c_device_code));
        if (!$camera_info) {

            return ['status' => -1, 'tips' => '摄像头信息不存在', 'data' => []];
        }

        $reuslt =  $this->update(array('c_device_status' => $c_device_status), "c_device_code='$c_device_code'");

        if ($reuslt) {
            
            return ['status' => 200, 'tips' => '更新成功', 'data' => $reuslt];
        }
        
        return ['status' => -1, 'tips' => '更新失败', 'data' => []];

    }

    /**
     * 海康获取第一个认证token
     * @return [type] [description]
     */
    public function haiKangFirstToken()
    {
        header("content-type:text/html;charset=utf-8");
        try {

            $client = new SoapClient($this->_loginWebLocation, array('encoding'=>'UTF-8'));
            // var_dump($client->__getFunctions());
            // print("<br/>");
            // var_dump($client->__getTypes());
            // print("<br/>");
          
            // $parm1 = "php client call";
            // $param = array('param0' => $parm1);
            $password = hash('sha256', HAIKANG_SERVER_PASSWORD);

            $loginParam = array('loginAccount'  => HAIKANG_SERVER_USERNAME, 
                                'password'      => $password,
                                'serviceIp'     => HAIKANG_SERVER_IP, 
                                'clientIp'      => $this->get_client_ip(),
                                'clientMac'     => '28:cf:e9:12:67:47'
            );
 
            $loginResult = $client->login($loginParam);
            $resultString = $loginResult->return;
            $info = simplexml_load_string($resultString);
            // print_r($info);exit;

            $token = '';
            if((string)$info->rows->row[0]['tgc']){
                $token = (string)$info->rows->row[0]['tgc'];
            }

            $this->_first_token = $token;

            return $token;

        } catch (SOAPFault $e) {

            return ['status' => -1, 'tips' => '一级token获取失败', 'data' => $e];
            // print $e;
        }
    }

    /**
     * 海康获取第二个认证token
     * [haiKangSecondToken description]
     * @return [type] [description]
     */
    public function haiKangSecondToken()
    {
        header("content-type:text/html;charset=utf-8");

        try {

            $client = new SoapClient($this->_resourceLocation, array('encoding'=>'UTF-8'));

            if (empty($this->_first_token)) {
                $this->haiKangFirstToken();
            }

            $param = array('tgt' => $this->_first_token);
 
            $loginResult = $client->applyToken($param);
            $resultString = $loginResult->return;
            $info = simplexml_load_string($resultString);

            $token = '';
            if((string)$info->rows->row[0]['st']){

                $token = (string)$info->rows->row[0]['st'];
            }

            if (empty($token)) {
            
                return ['status' => -1, 'tips' => '二级token获取失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }

            return ['status' => 200, 'tips' => '二级token获取成功', 'data' => $token];

        } catch (SOAPFault $e) {

            return ['status' => -1, 'tips' => '二级token获取失败', 'data' => $e];
            // print $e;
        }

    }

    /**
     * 获取所有的地址资源信息
     * @return [type] [description]
     */
    public function haiKangAllResource()
    {
        ini_set('max_execution_time', 600);// 最多执行十分钟
        header("content-type:text/html;charset=utf-8");

        try {

            $client = new SoapClient($this->_allResource, array('encoding'=>'UTF-8'));

            $secondTokenResult = $this->haiKangSecondToken();
            if ($secondTokenResult['status'] != 200) {

                return ['status' => -1, 'tips' => '地址资源获取失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }
            $secondToken = $secondTokenResult['data'];

            $param = array('token' => $secondToken, 'resType' => 1000, 'operCode' => '');
 
            // print_r($param);exit;
            $allResource = $client->getAllResourceDetailWithJson($param);
            $resultString = $allResource->return;
            $info = simplexml_load_string($resultString);

            $data = '';
            if((string)$info->rows->row[0]['data']){

                $data = (string)$info->rows->row[0]['data'];
            }
            if (empty($data)) {
            
                return ['status' => -1, 'tips' => '获取地址资源失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }

            return ['status' => 200, 'tips' => '获取地址资源成功', 'data' => json_decode($data, true)];

            // return json_decode($data, true);

        } catch (SOAPFault $e) {

            return ['status' => -1, 'tips' => '获取所有资源的webservice错误', 'data' => $e];
            // print $e;
        }
    }

    /**
     * 将地址转化为树形结构
     * @param  string $allAddress [description]
     * @return [type]             [description]
     */
    public function haiKangResourceToTree($allAddress = '')
    {
        // $resourceResult = $this->haiKangAllResource();
        // if ($resourceResult['status'] != 200) {
            
        //     return ['status' => -1, 'tips' => '转换树形结构是获取地址资源失败', 'data' => ''];
        // }
        // $arr = $resourceResult['data'];
        $arr = $allAddress;
        foreach ($arr as &$data) {
            $data['id'] = $data['i_id'];
            $data['parent_id'] = isset($data['i_parent_id']) ? $data['i_parent_id'] : 0;
            $data['name'] = $data['c_org_name'];

        }
        $treeData = $this->Address_model->getTree($arr, 0);
        return ['status' => 200, 'tips' => '转化成功', 'data' => $treeData];

        $treeHtml = $this->Address_model->procHtml($treeData);
        return $treeHtml;
    }


    /**
     * [importHaiKangCameras description]
     * @Remark 步骤
     * 1. 获取所有地址
     * 2. 将地址存入数据库
     *     2.1 降地址存入数据库
     *     2.2 获取存入数据库的地址id，将相关摄像头信息存入摄像头数据库
     * 
     * 获取所有地址格式
     * 
     * @param  string $type [description]
     * @return [type]       [description]
     */
    public function importHaiKangCameras()
    {
        ini_set('max_execution_time', 600);// 最多执行十分钟
        // 1. 获取所有地址
        $allAddressResult = $this->haiKangAllResource();

        if ($allAddressResult['status'] != 200) {
            
            return ['status' => -1, 'tips' => '导入摄像头信息获取地址失败', 'data' => ''];
        }
        $allAddress = $allAddressResult['data'];

        // 2.地址存入数据库
        $treeData = $this->haiKangResourceToTree($allAddress);
        if ($treeData['status'] != 200) {
            
            return ['status' => -1, 'tips' => '将摄像头地址转换为树行结构失败', 'data' => ''];
        }


        return $this->saveAddress($treeData['data']);
    }

    /**
     * 递归导入地址
     * @param  string  $treeData  [description]
     * @param  integer $parent_id [description]
     * @return [type]             [description]
     */
    public function saveAddress($treeData = '', $parent_id = 0) {
        
        ini_set('max_execution_time', 600);// 最多执行十分钟
        $total_count = $failed_count = 0;
        foreach ($treeData as $level) {
            // 2.1 地址存入数据库
            $level_result = $this->buildAddressParams($level['name'], $parent_id);
            list($level_address, $address_id) = $level_result;

            // 2.2 获取存入数据库的地址id，将相关摄像头信息存入摄像头数据库
            if (!empty($level['c_index_code'])) {
                $total_count++;

                $addResult = $this->saveCamerasByIndexCode($level['c_index_code'], $address_id);
                
                
                if (empty($addResult)) {
                    $failed_count++;
                }
                // error_log(date('Y-m-d H:i:s', time()).'  '.$level['c_org_name'].'导入成功'.json_encode($addResult)."\n\n", 3, LOG_PATH.'haiKangImport.log');
            }

            if (!empty($level['parent_id'])) {
                $this->saveAddress($level['parent_id'], $address_id);
            }  
        }

        return ['status' => 200, 'tips' => "导入成功", 'data' => ''];
    }


    /**
     * [saveCamerasByIndexCode 查找某个地址下的所有摄像头，并存入数据库
     * @param  string  $orgCode    [description]
     * @param  integer $address_id [description]
     * @return [type]              [description]
     */
    public function saveCamerasByIndexCode($indexCode = '', $address_id = 0)
    {
        // $indexCode = '00000011005000000001';
        // $address_id = 128;
        header("content-type:text/html;charset=utf-8");

        try {

            $client = new SoapClient($this->_allResource, array('encoding'=>'UTF-8'));

            $secondTokenResult = $this->haiKangSecondToken();
            if ($secondTokenResult['status'] != 200) {

                return ['status' => -1, 'tips' => '保存资源时获取二级token失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }

            $secondToken = $secondTokenResult['data'];
            $param = array('token' => $secondToken, 'orgCode' => $indexCode, 'resType' => 10000, 'operCode' => '10001');
            
            $allResource = $client->getAllResourceDetailByOrg($param);
           
            $resultString = $allResource->return;
            $info = simplexml_load_string($resultString);

            // return (string)$info->rows;
            $infoRows = (string)$info->rows;
            if (empty($infoRows)) {
                
                return ['status' => -1, 'tips' => "该indexCode $indexCode 没有摄像头信息", 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }
            $i = 0;
            $total_count = $failed_count = 0;
            foreach ($info->rows->row as $camera) {
                
                $add_params = [];
                $cameraIndexCode = '';
                $total_count++;

                $cameraIndexCode = (string)$camera[0]['c_index_code'];
                $add_params = array(
                        'camera_name'   => (string)$camera[0]['c_name'],
                        'provider'      =>  'HAIKANG', 
                        'camera_type'   =>  (int)$camera[0]['i_camera_type'],
                        'c_index_code'  => $cameraIndexCode,
                        'custom_id'     => (string)$camera[0]['c_device_index_code'],//设备编号
                        'address_id'    => $address_id, 
                       
                        'longitude'     => (string)$camera[0]['c_longitude'],
                        'latitude'      => (string)$camera[0]['c_latitude'],
                        'c_index_code'  => (string)$camera[0]['c_index_code'],
                        'i_channel_no'  => (int)$camera[0]['i_channel_no'],
                        'i_ptz_type'    => (int)$camera[0]['i_ptz_type'],
                        'i_status'      => (int)$camera[0]['i_status'],
                        'i_is_online'   => (int)$camera[0]['i_is_online'],
                );
            // var_dump($add_params);exit;

                $cameraStreamInfo = $this->getCameraStreamServiceByNetZone($cameraIndexCode);

                if ($cameraStreamInfo['status'] == 200) {
                    $add_params['vtm_ip'] = $cameraStreamInfo['data']['vtm_ip'];
                    $add_params['vtm_port'] = $cameraStreamInfo['data']['vtm_port'];
                    $add_params['vtm_rtsp_port'] = $cameraStreamInfo['data']['vtm_rtsp_port'];
                    $add_params['vag_port'] = $cameraStreamInfo['data']['vag_port'];
                }

                $isCameraExits = $this->get_one([
                    'custom_id' => (string)$camera[0]['c_device_index_code'],
                    'c_index_code' => $cameraIndexCode
                ]);

                if ($isCameraExits) {
                    $operation = 'update';
                    $camera_id = $isCameraExits['id'];
                    $result = $this->update($add_params, 'id='.$camera_id);
                }else{
                    $operation = 'add';
                    $result = $this->insert($add_params, 1);
                    $camera_id = $result;
                }

                // error_log(date('Y-m-d H:i:s', time())."\tid=".$camera_id."\t"."operation=".$operation."\t"."村子indexCode=".$indexCode."\t".json_encode($add_params)."\n\n", 3, LOG_PATH.'add_param.log');


                if (empty($result)) {
                    
                    $failed_count++;
                }

            }
            return array('status'=> 200,'tips'=>"总共 $total_count 个 失败 $failed_count 个", 'data' => ['total_count' => $total_count, 'failed_count' => $failed_count]);

        } catch (SOAPFault $e) {

            return array('status'=> -1,'tips'=>"新增摄像头失败", 'data' => $e);

        }
    }


    /**
     * 组装某个摄像头的vtm信息
     * @param  string $cameraIndexCode [description]
     * @return [type]                  [description]
     */
    public function getCameraStreamServiceByNetZone($cameraIndexCode = '')
    {
        $getAllResource = 'http://'.HAIKANG_SERVER_IP.':'.HAIKANG_SERVER_PORT.'/vms/services/VmsCuService?wsdl';

        header("content-type:text/html;charset=utf-8");

        try {

            $client = new SoapClient($getAllResource, array('encoding'=>'UTF-8'));

            $secondTokenResult = $this->haiKangSecondToken();
            if ($secondTokenResult['status'] != 200) {

                return ['status' => -1, 'tips' => '地址资源获取失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }
            $secondToken = $secondTokenResult['data'];
            $param = array('token' => $secondToken, 'type' => 1, 'cameraIndexCode' => $cameraIndexCode, 'netZoneId' => 2);
 
            $allResource = $client->getCameraStreamServiceByNetZone($param);
            $resultString = $allResource->return;
            $info = simplexml_load_string($resultString);

            // print_r($info->rows);exit;

            $result = [];
            foreach ($info->rows->row as $row) {
                if ((string)$row[0]['name'] == 'vtm') {

                    $result['vtm_ip'] = (string)$row[0]['ip'];
                    $result['vtm_port'] = (string)$row[0]['port'];
                    $result['vtm_rtsp_port'] = (string)$row[0]['rtsp_port'];
                }
                if ((string)$row[0]['name'] == 'vag') {

                    $result['vag_port'] = (string)$row[0]['port'];
                }
            }
            
            if (empty($result)) {
            
                return ['status' => -1, 'tips' => '获取CameraStream失败', 'data' => '方法'.__FUNCTION__.'第'.__LINE__.'行'];
            }

            return ['status' => 200, 'tips' => '获取CameraStream成功', 'data' => $result];

            // return json_decode($data, true);

        } catch (SOAPFault $e) {

            return ['status' => -1, 'tips' => '获取所有资源的webservice错误', 'data' => $e];
            // print $e;
        }
    }

    private function buildAddressParams($address_name = '', $parent_id = 0)
    {
        $is_name_exsit = $this->Address_model->isNameExist($address_name);

        // 一级地址存在的话就用存在的，否则就添加为新的，其下同理
        if ($is_name_exsit) {
            
            $address = $is_name_exsit;
            $address_id = $is_name_exsit['id'];
        } else {
            
            $address = $this->Address_model->add(['name' => $address_name, 
                                                  'parent_id' => $parent_id]
                                                );
            $address_id = $address['status'] ? $address['data'] : 0;
        }

        return [$address, $address_id];
    }



    public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    
    /*
    *    默认信息
    */
    function default_info(){

        return array(
            'camera_name'   => "",
            'custom_id'     => "",
			'provider'      => 'DAHUA',
            'camera_type'   => '',
            'longitude'     => '',
            'latitude'      => '',
            'email'         => "",
            'mobile'        => "",
            'c_device_index_code' => '',
            'i_is_online'   => 1,
            'user_id'       => 0,
            'vtm_ip'        => '',
            'vtm_port'      => '',
            'vtm_rtsp_port' => '',
            'vag_port'      => '',

        );
    }
}
