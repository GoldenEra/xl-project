<?php 
class Material_type_model extends Base_Model {
    public function __construct() {

        // 表前缀
        $this->table_name = 'material_type';

        $this->load->library('tree');
        parent::__construct();
        $this->load->model(array('Material_model'));

    }

    /**
     * 获取所有分类列表
     * 
     * @return [type]      [description]
     */
    public function getMaterialTypeList()
    {
        return $this->select('', '*');
    }

    public function getLevelOntList()
    {
        return $this->select('level=1 and parent_id=0', '*');
    }

    public function getByLevelOneID($levelOneID = 0)
    {
        return $this->select('level=2 and parent_id='.$levelOneID, '*');
    }

    /**
     * 增加分类列表
     * @param array $params [description]
     */
    public function add($params = [])
    {
        if (empty($params)) {
            
            return array('status' => 200,'tips' => "ok");
        }
        $parent_id = trim($params['parent_id']);
        if (empty($parent_id)) {

            $params['level'] = 1;
            $params['parent_id'] = 0;
        } else {

            $parent_detail = $this->get_one(['id' => $params['parent_id']]);
            if (!$parent_detail) {

                return array('status' => -1,'tips' => "父分类不存在");
            }

            $params['level'] = $parent_detail['level'] + 1;
        }

        if ($params['level'] > 2) {

                return array('status' => -1,'tips' => "最多只能添加二级分类，请重新选择父分类");
        }
        
        $insert_id = $this->insert($params);

        if (empty($insert_id)) {
            
            return array('status'=> -1,'tips'=>"新增分类失败");
        }

        return array('status' => 200,'tips'=>"新增分类成功", 'data' => $insert_id);
    }

    public function formateMaterialTypeTree($material_type_list = [], $id = 0)
    {

        $str  = "<option depth='\$depth' value='\$id' \$selected>\$spacer \$name </option>";
        
        if (!empty($id)) {
            foreach($material_type_list as &$materialType) {

                $materialType['selected'] = $materialType['id'] == $id ? 'selected' : '';
            }
        }

        $tree = $this->tree;
        $tree->init($material_type_list);

        return $tree->get_tree(0, $str);
    }



    /**
     * 增加地址列表
     * @param array $update_field [description]
     */
    public function updateMaterialType($id = 0, $update_field = [])
    {
        if (empty($update_field)) {
            
            return array('status' =>  -1,'tips' => "更新记录为空");
        }

        $current = $this->get_one('id='.$id);
        if (empty($current)) {
            
            return array('status' =>  -1,'tips' => "未找到记录");
        }

        $updateReuslt = $this->update($update_field, 'id='.$id);
        if ($updateReuslt) {
            
            return array('status' => 200,'tips' => "更新成功");
        }

        return array('status' =>  -1,'tips' => "更新失败");
    }


    public function delete_material_type($material_type_ids = [])
    {
        if (empty($material_type_ids)) {

            return array('status'=> -1,'tips'=>"未选中任何记录");
        }

        foreach ($material_type_ids as $material_type_id) {

            $allIDs = [$material_type_id];
            // 如果是父分类，则删除父分类下的所有子分类
            $hasChild = $this->select(['parent_id' => $material_type_id]);
            if ($hasChild) {
                
                $childIDs = array_column($hasChild, 'id');
                $allIDs = array_merge($allIDs, $childIDs);
            }

            $ids = implode(',', $allIDs);
            // 删除分类时材料的分类也一并删除
            $this->Material_model->delete('material_type_id in ('.$ids.')');
            $this->delete('id in ('.$ids.')');
        }

        return array('status' => 200,'tips'=>"删除成功");

    }

    public function isLevelTwo($type_id = 0)
    {
        $result = $this->get_one(['id' => $type_id]);
        if (!$result) {
            
            return array('status'=> -1,'tips'=>"未找到该分类");
        }

        if ($result['level'] != 2) {
            
            return array('status'=> -1, 'tips'=>"分类选择不正确");
        }

        return array('status'=> 200, 'data' => $result, 'tips'=>"分类选择正确");
    }

    public function isNameExist($name = '')
    {
        return $this->get_one("name='$name'");
    }

    /*
    *    默认信息
    */
    function default_info() {

        return array(
            'name' => "",
            'level' => "",
            'desc' => "",
            'parent_id' => 0,
        );
    }

}
