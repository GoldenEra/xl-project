<?php 
class Address_model extends Base_Model {
    public function __construct() {

        // 表前缀
        $this->table_name = 'address';

        $this->load->library('tree');
        parent::__construct();
        $this->load->model(array('Role_camera_model'));
    }

    /**
     * 获取所有地址列表
     * 
     * @return [type]      [description]
     */
    public function getAddressList()
    {
        return $this->select('', '*');
    }

    public function getParentIDs($id = 0)
    {
        $parent_ids = [];

        if (empty($id)) {
            return $parent_ids;
        }

        $maxCount = 50;
        $i = 0;
        $level = 0;
        do {
            $i++;
            if ($i > $maxCount) {
                
                return $parent_ids;
            }
            $result = $this->get_one("id=".$id);
            if (!$result) {
                continue;
            }
            $parent_id = $result['parent_id'];
            if (empty($parent_id)) {
                break;
            }
            $id = $parent_id;
            $level = $result['level'];
            $parent_ids[] = $id;
            
            
        } while ($level != 1);

        return $parent_ids;
    }

    public function getChildIDs($id = 0)
    {
        $child_ids = [];

        if (empty($id)) {
            return $child_ids;
        }

        $maxCount = 50;
        $i = 0;
        $ids = array($id);
        do {

            $where = '('. implode(',', $ids).')';
            $result    = $this->select("parent_id in ".$where);
            if (empty($result)) {
                break;
            }
            $ids = array_column($result, 'id');
            $child_ids = array_merge($child_ids, $ids);
            $i++;
            if ($i > $maxCount) {
                
                return $child_ids;
            }
            
        } while (true);

        return $child_ids;
    }

    
    public function getAddressDetailByID($id = 0)
    {
        if (empty($id)) {
            return '';
        }
        $parent_ids = $this->getParentIDs($id);

        $allIDs = array_merge($parent_ids, [$id]);

        $where = implode(',', $allIDs);
        $address = $this->select('id in ('.$where.')', '*', '', 'level', '');

        $detail = '';
        $length = count($address);
        for ($i = 0; $i < $length; $i++) { 

            $detail .= $address[$i]['name'];
            if ($i != $length - 1) {
                $detail .= ' -- ';
            }
        }
        foreach ($address as $key => $value) {
            
        }

        return $detail;
    }


    /**
     * 增加地址列表
     * @param array $params [description]
     */
    public function add($params = [])
    {
        if (empty($params)) {
            
            return json_encode(array('status' => true,'tips' => "ok"));
        }

        if (empty($params['parent_id'])) {
            # code...
            $params['level'] = isset($params['level']) ? $params['level'] : 1;
            $params['parent_ids'] = 0;
        } else {

            $parent_ids = $this->getParentIDs($params['parent_id']);
            $parent_ids = array_merge($parent_ids, [$params['parent_id']]);
            
            if (!empty($parent_ids)) {

                $params['parent_ids'] = implode(',', $parent_ids);
                $params['level'] = count($parent_ids) + 1;
            }
        }
        
        $insert_id = $this->insert($params);
        if (empty($insert_id)) {
            
            return array('status'=>false,'tips'=>"新增地址失败");
        }

        return array('status' => true,'tips'=>"新增地址成功", 'data' => $insert_id);
    }

    /**
     * 增加地址列表
     * @param array $update_field [description]
     */
    public function update_address($id = 0, $update_field = [])
    {
        if (empty($update_field)) {
            
            return array('status' => false,'tips' => "更新记录为空");
        }

        // 更新name 和 zipcode
        $current = $this->get_one('id='.$id);
        if (empty($current)) {
            
            return array('status' => false,'tips' => "未找到记录");
        }

        $this->update($update_field, 'id='.$id);

        // 如果parent_id 没更新，则无需更新子节点数据
        if ($current['parent_id'] == $update_field['parent_id']) {
            
            return array('status'=>true,'tips'=>"更新成功");
        }

        $childIDs = $this->getChildIDs($id);

        $allIDs = array_merge([$id], $childIDs);
        
        foreach ($allIDs as $key => $val) {
            
            $parent_ids = $this->getParentIDs($val);
            $params = [];
            if (!empty($parent_ids)) {

                $params['parent_ids'] = implode(',', $parent_ids);
                $params['level'] = count($parent_ids) + 1;
            }
            $this->update($params, 'id='.$val);
        }

        return array('status' => true, 'tips' => '更新失败');

    }

    public function delete_address($address_ids = [])
    {
        if (empty($address_ids)) {

            return array('status'=>false,'tips'=>"未选中任何记录");
        }

        $address_ids = is_array($address_ids) ? $address_ids : [$address_ids];

        foreach ($address_ids as $address_id) {

            $childIDs = $this->getChildIDs($address_id);
            $allIDs = array_merge($childIDs, [$address_id]);
            $ids = implode(',', $allIDs);
            
            $this->delete('id in ('.$ids.')');
        }

        return array('status'=>true,'tips'=>"删除成功");

    }

    public function toTree($address_list = '', $parent_id = 0)
    {
        $tree = $this->getTree($address_list, $parent_id);
        return $this->Address_model->procHtml($tree);
    }

    public function formateAddressTree($address_list = [], $id = '')
    {


        $str  = "<option depth='\$depth' value='\$id' \$selected>\$spacer \$name </option>";
        
        if (!empty($id)) {
            foreach($address_list as &$address) {

                $address['selected'] = $address['id'] == $id ? 'selected' : '';
            }
        }

        $tree = $this->tree;
        $tree->init($address_list);

        return $tree->get_tree(0, $str);
    }

    public function FunctionName($value='')
    {
        # code...
    }

    public function getTree($data, $pId)
    {
        $tree = [];

        foreach($data as $k => $v)
        {
             if($v['parent_id'] == $pId)
             { //父亲找到儿子
                 $v['parent_id'] = $this->getTree($data, $v['id']);
                 $tree[] = $v;
                 //unset($data[$k]);
             }
        }
        return $tree;
    }

    public function saveAddress($treeData, $pid)
    {
        
    }

    function procHtml($tree)
    {
        $html = '';
        foreach($tree as $t)
        {   
            $t['name'] = $this->security->xss_clean($t['name']);
            if($t['parent_id'] == '')
            {
                $html .= "<li id='tree_{$t['id']}' data-id={$t['id']}>{$t['name']}</li>";
            }
            else
            {   
                $html .= "<li id='tree_{$t['id']}' data-id={$t['id']}>".$t['name'];
                $html .= $this->procHtml($t['parent_id']);
                $html .= "</li>";
            }
        }

        return $html ? '<ul>'.$html.'</ul>' : $html ;
    }


    public function toTreeWithCameraNum($address_list = '', $parent_id = 0, $role_id = 0)
    {
        $tree = $this->getTree($address_list, $parent_id);
        return $this->Address_model->procHtmlWithCameraNum($tree, $role_id);
    }

    public function procHtmlWithCameraNum($tree, $role_id)
    {
        $html = '';
        $where = '';
        foreach($tree as $t)
        {   
            $allCamera = $this->getCameraCount($t['id'], $role_id);
            $cameraCount = $allCamera[0];
            $choosenCount = $allCamera[1];
            $t['name'] = $this->security->xss_clean($t['name']);
            if($t['parent_id'] == '')
            {
                $html .= "<li data-id={$t['id']}>{$t['name']}({$choosenCount}/{$cameraCount})</li>";
            }
            else
            {   
                $html .= "<li data-id={$t['id']}>".$t['name'];
                $html .= "($choosenCount/$cameraCount)";
                $html .= $this->procHtmlWithCameraNum($t['parent_id'], $role_id);
                $html .= "</li>";
            }
        }

        return $html ? '<ul>'.$html.'</ul>' : $html ;
    }

    public function getCameraCount($address_id = 0, $role_id = 0)
    {
        $childIDs = $this->Address_model->getChildIDs($address_id);
        $allIDs = array_merge($childIDs, array($address_id));
        $where = 'address_id in ('. implode(',', $allIDs).')';

        $cameraCount = $choosenCount = 0;
        $cameraCount = $this->Camera_model->count($where);
        $roleWhere = $where.' and role_id='.$role_id.' and is_del=1';
        $choosenCount = $this->Role_camera_model->count($roleWhere);

        return array($cameraCount, $choosenCount);
    }


    public function isNameExist($name = '')
    {
        return $this->get_one("name='$name'");
    }

}
