<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 报警信息处理日志类
 */
class Message_log_model extends Base_Model {
    
    public function __construct() {

        $this->db_tablepre = 't_sys_';
        $this->table_name = 'message_log';
        parent::__construct();
    }

    public function addLog($message_id = 0, $status = '', $user_id = 0)
    {
    	return $this->insert(array(
                                        'message_id' => $message_id,
                                        'status' => $status,
                                        'staff_id' => $user_id,
        ));
    }

 }