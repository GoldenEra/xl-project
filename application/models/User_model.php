<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*/
class User_model extends Member_model {
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Address_model'));
        $this->load->model(array('Camera_model'));
        $this->load->model(array('Role_camera_model'));
        $this->load->model(array('Member_model'));
        $this->load->model(array('Message_model'));
        $this->load->model(array('Member_role_priv_model'));
    }


    /**
     * 用户弹窗
     * @return array
     */
    function user_window_datasource($where='',$limit = '', $order = '', $group = '', $key=''){
    	
    	$datalist = $this->select($where,'user_id,username',$limit,$order,$group,$key);
        return $datalist;
    }

     /**
     * 用户下拉框
     * @return array
     */
    function user_dropdown_datasource($where='',$limit = '', $order = '', $group = '', $key=''){
        
        $datalist = $this->select($where,'user_id,username',$limit,$order,$group,$key);
        return $datalist;
    }

    /**
     * 用户下拉值
     * @return array
     */
    function user_dropdown_value($id){
        
        $datainfo = $this->get_one(array('user_id'=>$id));
        return isset($datainfo['username'])?$datainfo['username']:'-';
    }

    /**
     * 用户登陆接口
     * @param  string $username [description]
     * @param  string $password [description]
     * @return [type]           [description]
     */
    public function userLogin($username = '', $password = '')
    {
        if (empty($username) || empty($password)) {
            
            return ['status' => -1, 'tips' => '账号或密码为空', 'data' => []];
        }

        $result = $this->Member_model->get_one(array('mobile'=>$username));



        if(!$result) {

            return ['status' => -1, 'tips' => '该用户不存在', 'data' => []];
        }
        
        $client_password = md5(md5(trim($password).$result['encrypt']));

        if ($result['password'] != $client_password) {
            
            return ['status' => -1, 'tips' => '密码不正确', 'data' => []];
        }
        $user_id = $result['user_id'];
        $result['toLeader'] = 0;
        $result['updateMessageStatus'] = 0;

        $privLeader = $this->User_model->checkPriv($user_id, 'toLeader');
        if ($privLeader['status'] == 200) {
            $result['toLeader'] = 1;
        }

        $privLeader = $this->User_model->checkPriv($user_id, 'updateMessageStatus');
        if ($privLeader['status'] == 200) {
            $result['updateMessageStatus'] = 1;
        }
        if ($result['group_id'] == SUPERADMIN_GROUP_ID) {
            $result['toLeader'] = 1;
            $result['updateMessageStatus'] = 1;
        }


        return ['status' => 200, 'tips' => '登陆成功', 'data' => $result];
    }


    public function userMobileLogin($mobile = '')
    {

        if (!empty($mobile)) {

            $result = $this->User_model->get_one(['mobile' => $mobile]);
            if ($result) {
                
                $user_id = $result['user_id'];
                $result['toLeader'] = 0;
                $result['updateMessageStatus'] = 0;

                $privLeader = $this->User_model->checkPriv($user_id, 'toLeader');
                if ($privLeader['status'] == 200) {
                    $result['toLeader'] = 1;
                }

                $privLeader = $this->User_model->checkPriv($user_id, 'updateMessageStatus');
                if ($privLeader['status'] == 200) {
                    $result['updateMessageStatus'] = 1;
                }
                if ($result['group_id'] == SUPERADMIN_GROUP_ID) {
                    $result['toLeader'] = 1;
                    $result['updateMessageStatus'] = 1;
                }
                
                return ['status' => 200, 'tips' => '登陆成功', 'data' => $result];
            }

            return ['status' => -1, 'tips' => '用户不存在', 'data' => []];
        }

            
        return ['status' => -1, 'tips' => '未查到用户号码', 'data' => []];

    }

    public function addUser($username = '', $password = '', $id_card = '', 
                            $mobile = '',$address_id = 1, $address_detail = '')
    {
        // var_dump(func_get_args());exit;
        $username = trim($username);
        $password = trim($password);
        if (empty($username) || empty($password)) {
            
            return ['status' => -1, 'tips' => '账户和密码不能为空', 'data' => ''];
        }

        if(strlen(trim($id_card)) != 18) {
            return ['status' => -1, 'tips' => '身份证格式不正确', 'data' => ''];
        }

        $address_id = trim($address_id);
        $address_detail = trim($address_detail);
        if (empty($address_id) || empty($address_detail)) {
            
            return ['status' => -1, 'tips' => '地址信息出错', 'data' => ''];
        }

        if(!is_mobile($mobile)) {

            return ['status' => -1, 'tips' => '手机号格式不正确', 'data' => ''];
        }

        // 用户名不能重复
        $isUsername = $this->get_one(['username' => $username]);
        if ($isUsername) {
            
            return ['status' => -1, 'tips' => '该用户名已经存在', 'data' => ''];
        }

        // 手机号不能重复
        $isMobile = $this->get_one(['mobile' => $mobile]);
        if ($isMobile) {
            
            return ['status' => -1, 'tips' => '该手机号已经存在', 'data' => ''];
        }

        // 身份证不能重复
        $isIdcard = $this->get_one(['id_card' => $id_card]);
        if ($isIdcard) {
            
            return ['status' => -1, 'tips' => '该身份证已经存在', 'data' => ''];
        }

        // 验证地址
        if(!$this->Address_model->get_one(['id' => $address_id])) {

            return ['status' => -1, 'tips' => '地址不存在', 'data' => ''];
        }

        $encrypt = random_string('alnum',5);
        $password = md5(md5($password.$encrypt));

        $new_id = $this->Member_model->insert(
                                                array(
                                                    'username'  =>$username,
                                                    'password'  =>$password,
                                                    // 'group_id'  =>$group_id,
                                                    // 'email'     =>$email,
                                                    'id_card'   =>$id_card,
                                                    'address_id'=>$address_id,
                                                    'address_detail'=>$address_detail,
                                                    'mobile'    =>$mobile,
                                                    // 'fullname'  =>$fullname,
                                                    // 'is_lock'   =>$is_lock,
                                                    // 'avatar'    =>$thumb,
                                                    'reg_time'  =>date('Y-m-d H:i:s'),
                                                    'encrypt'   =>$encrypt,
                                                    'reg_ip'    =>$this->input->ip_address(),
                                            ));
        if($new_id) {

            $result = $this->Member_model->get_one(array('user_id'=>$new_id));
            return ['status' => 200, 'tips' => '新增成功', 'data' => $result];

        }else {

            return ['status' => -1, 'tips' => '新增用户失败', 'data' => ''];
        }
    }

    /**
     * 客户端报警
     * @param  integer $repoter_user_id [description]
     * @param  string  $type            [description]
     * @param  string  $content         [description]
     * @param  string  $latitude        [description]
     * @param  string  $longitude       [description]
     * @param  string  $image           [description]
     * @param  string  $video           [description]
     * @param  string  $thumb           [description]
     * @return [type]                   [description]
     */
    public function callPolice($repoter_user_id = 0, $type = '', $content = '', 
                               $latitude = '', $longitude = '', $address_detail = '',
                               $image = '',  $video = '', $thumb = '')
    {
        
        if (empty($repoter_user_id)) {
            
            return ['status' => -1, 'tips' => '用户为空', 'data' => []];
        }

        $user_info = $this->get_one(array('user_id'=>$repoter_user_id));
        if (!$user_info) {
            
            return ['status' => -1, 'tips' => '用户不存在', 'data' => []];
        }

        // 新建报警信息
        $result_id = $this->Message_model->create($repoter_user_id, $type, $content, $latitude, $longitude, $address_detail, $image, $video, $thumb);

        if ($result_id) {
            $message = $this->Message_model->get_one(['id' => $result_id]);
            return ['status' => 200, 'tips' => '报警信息创建成功', 'data' => $message];
        }

    }

    /**
     * 信息上报
     * @param  integer $message_id [description]
     * @param  integer $user_id    [description]
     * @return [type]              [description]
     */
    public function toLeader($message_id = 0, $user_id = 0)
    {
        if (empty($message_id)) {
            
            return ['status' => -1, 'tips' => '信息为空', 'data' => []];
        }
        $user_id = $this->user_id;

        return $this->Message_model->toLeader($message_id, $user_id);
    }

    public function fnEncrypt($sValue, $sSecretKey)
    {
        return rtrim(
            base64_encode(
                mcrypt_encrypt(
                    MCRYPT_RIJNDAEL_128,
                    $sSecretKey, $sValue, 
                    MCRYPT_MODE_ECB, 
                    mcrypt_create_iv(
                        mcrypt_get_iv_size(
                            MCRYPT_RIJNDAEL_128, 
                            MCRYPT_MODE_ECB
                        ), 
                        MCRYPT_RAND)
                    )
                ), "\0"
            );
    }

    public function fnDecrypt($sValue, $sSecretKey)
    {
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128, 
                $sSecretKey, 
                base64_decode($sValue), 
                MCRYPT_MODE_ECB,
                mcrypt_create_iv(
                    mcrypt_get_iv_size(
                        MCRYPT_RIJNDAEL_128,
                        MCRYPT_MODE_ECB
                    ), 
                    MCRYPT_RAND
                )
            ), "\0"
        );
    }


    /**
     * 检查权限
     * 由于权限检查是写在controller层的，而接口是直接调用的model层
     * 如果要检查接口的权限，则必须保持controller层和model层的方法
     * 名一致
     * 
     * @param  integer $user_id     [description]
     * @param  string  $method_name [description]
     * @return [type]               [description]
     */
    public function checkPriv($user_id = 0, $method_name = '')
    {
        $user_info = $this->get_one(array('user_id'=>$user_id));
        if (!$user_info) {
            
            return ['status' => -1, 'tips' => '用户不存在', 'data' => []];
        }
        $role_id = $user_info['group_id'];

		if ($role_id == SUPERADMIN_GROUP_ID) {
        	return ['status' => 200, 'tips' => '有操作权限', 'data' => ''];
        }

        $priv = $this->Member_role_priv_model->get_one(array('method'=> $method_name, 'role_id' => $role_id));

        if (!$priv) {
            
            return ['status' => -1, 'tips' => '没有操作权限', 'data' => []];
        }

        return ['status' => 200, 'tips' => '有操作权限', 'data' => $priv];
    }

    public function normalizeString($str = '')
    {
		$str = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $str);
		// Remove any runs of periods
		$str = mb_ereg_replace("([\.]{2,})", '', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|\~\)\(\)]+/', '', $str);
       	return $str;
    }

}
