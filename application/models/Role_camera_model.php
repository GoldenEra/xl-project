<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*/
class Role_camera_model extends Base_Model {

    public function __construct() {


        $this->table_name = 'member_role_camera';
        
        $this->load->model(array('Camera_model'));
        $this->load->model(array('Member_role_model'));
        parent::__construct();
    }

    public function insert_role_camera($role_id = 0, $camera_ids = [])
    {
        $data_info =$this->Member_role_model->get_one(array('role_id'=>$role_id));
        
        if(!$data_info) {
            return array('status' => false, 'tips' => '该角色不存在');
        }

        foreach ($camera_ids as $camera_id) {
            $camera_info = $this->Camera_model->get_one(array('id' => $camera_id));
            if (!$camera_info) {
                continue;
            }
            $address_id = $camera_info['address_id'];

            $insert_arr[] = ['role_id' => $role_id, 'camera_id' => $camera_id, 'address_id' => $address_id];
        }

        return $this->insert_batch($insert_arr);
    }

    /**
     * 用户是否拥有查看某个摄像头的资格（权限）
     * 
     * @param  integer $user_id   [用户ID]
     * @param  integer $camera_id [摄像头ID]
     * @return [type]             [description]
     */
    public function userHasPermission($user_id = 0, $camera_id = 0)
    {
        if (empty($user_id) || empty($camera_id)) {
            
            return false;
        }

        $user_info = $this->get_one(array('user_id'=>$id));
        if (!$user_info) {
            
            return false;
        }
        $user_role_id = $user_info['group_id'];

        return $this->get_one(['role_id' => $user_role_id, 'camera_id' => $camera_id]);

    }

    public function roleHasPermission($group_id = 0, $camera_id = 0)
    {
        return $this->get_one(['role_id' => $group_id, 'camera_id' => $camera_id]);
    }

    public function delete_by_role($role_id = 0, $camera_ids = [])
    {
        $data_info =$this->Member_role_model->get_one(array('role_id'=>$role_id));
        
        if(!$data_info) {
            return array('status' => false, 'tips' => '该角色不存在');
        }

        $where = implode(',', $camera_ids);
    

        return $this->delete('role_id='.$role_id.' and camera_id in ('. $where .')');
    }

}