<?php 
class SMS_send_model extends Base_Model {
    public function __construct() {

        parent::__construct();
    }

    /**
     * 创建url
     */
    private function createUrl()
    {
        return SMS_BASE_URL;
    }

    private function createSig()
    {
        // 时间戳
        date_default_timezone_set("Asia/Shanghai");
        $timestamp = date("YmdHis");

        // 签名
        return md5(SMS_ACCOUNT_SID . SMS_AUTH_TOKEN . $timestamp);
    }

    private function createBasicAuthData()
    {
        $timestamp = date("YmdHis");
        // 签名
        $sig = $this->createSig();
        return array("accountSid" => SMS_ACCOUNT_SID, "timestamp" => $timestamp, "sig" => $sig, "respDataType"=> "JSON");
    }

    /**
     * 创建请求头
     * @param body
     * @return
     */
    private function createHeaders()
    {
        return array('Content-type: ' . SMS_CONTENT_TYPE, 'Accept: ' . SMS_ACCEPT);
    }

    private function addSMSLog($result = '', $number = '') {

        if ($result === FALSE) {
            
            $response = [];
        }else{

            $response = json_decode($result, true);
            if (!is_array($response)) {
                $response = [];
            }
        }
        // 返回码参见 http://www.miaodiyun.com/doc/status_code.html
        if (isset($response['respCode']) && $response['respCode'] == 200) {
            $arr['msg'] = 'send success';
        } else {
            $arr['msg'] = 'send fail';
            // 记录下失败的返回信息
        }

        $arr['time'] = date('Y-m-d H:i:s', time());
        $arr['number'] = $number;
        $arr = array_merge($response, $arr);

        error_log(json_encode($arr)."\n\n", 3, LOG_PATH.'sms_send.log');
    }

    public function sendSMS($number = '')
    {
        $success = $fail = 0;
        $number = is_array($number) ? $number : [$number];

        foreach ($number as $singleNumber) {
            $result = $this->sendSMSOne($singleNumber);
            if(!empty($result)) {
                $success++; 
            }
        }

        return '成功'.$success.'个';
    }

    /**
     * post请求
     *
     * @param funAndOperate
     *            功能和操作
     * @param body
     *            要post的数据
     * @return
     * @throws IOException
     */
    public function sendSMSOne($number = '')
    {
        
        // $number = is_array($number) ? $number : array($number);
        //$phone = implode(",", $number);
        // 构造请求数据
        $url = $this->createUrl();
        $headers = $this->createHeaders();

        // 生成body
        $body = $this->createBasicAuthData();
        // 在基本认证参数的基础上添加短信内容和发送目标号码的参数
       // $body['msg'] = SMS_CONTEND;
       // $body['phone'] = $phone;

        // 要求post请求的消息体为&拼接的字符串，所以做下面转换
        // $fields_string = "";
        // foreach ($body as $key => $value) {
        //     $fields_string .= $key . '=' . $value . '&';
        // }
        // rtrim($fields_string, '&');

        // 提交请求
        //$con = curl_init();
        //curl_setopt($con, CURLOPT_URL, $url);
        //curl_setopt($con, CURLOPT_SSL_VERIFYHOST, FALSE);
        //curl_setopt($con, CURLOPT_SSL_VERIFYPEER, FALSE);
        //curl_setopt($con, CURLOPT_HEADER, 0);
        // curl_setopt($con, CURLOPT_POST, 1);
       // curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        //curl_setopt($con, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($con, CURLOPT_POSTFIELDS, $fields_string);
        //$result = curl_exec($con);
        //curl_close($con);

	$curl = curl_init();
	
	$smscontent = curl_escape($curl, SMS_CONTEND);
        $url .= "?phone=".$number."&msg={$smscontent}";

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);
        curl_close($curl);

        $this->addSMSLog($result, $number);

        return $result;
    }


}
