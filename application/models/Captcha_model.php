<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Captcha_model extends Base_Model {

	public function __construct() {
		$this->table_name = 'captcha';
		parent::__construct();
		$this->load->helper('captcha');
	}

	public function generate_captcha($is_front = 0)
	{
		$vals = array(
		        'img_path'      => './captcha/',
		        'img_url'       => base_url().'captcha',
		        'font_path' 	=> FCPATH.'font/a_TrianglerCmUp.ttf',
		        // 'img_width'     => '80',
		        // 'img_height'    => '30',
		        'expiration'    => 7200,
		        'word_length'   => 4,
				'font_size'		=> 18,
		        'img_id'        => 'Imageid',

		        // White background and border, black text and red grid
		        'colors'        => array(
		                'background' => array(255, 255, 255),
		                'border' => array(255, 255, 255),
		                'text' => array(0, 0, 0),
		                'grid' => array(255, 40, 40)
		        )
			);

			$capData = create_captcha($vals);
			if (!$capData) {

				return array('status' => -1,'tips'=>"生成验证码失败", 'data' => '');
			}
			$this->session->set_userdata('captcha', $capData['word']);
			$filename = base_url().'captcha/'.$capData['filename'];
			return array('status' => 200,'tips'=>"生成验证码成功", 'data' => $filename, 'captcha' => $capData['word']);
	}
	
	function save_captcha($data) {
		$this->insert($data);
	}
	
	function delete_old_captcha($expiration) {
	    $query = $this->delete( array("captcha_time"=>$expiration));
	}
	
	function verify_captcha($word,$ip_address,$captcha_time) {
		
	    $c = $this->count(array("word"=>$word,"ip_address"=>$ip_address,"captcha_time > "=>$captcha_time));

	    if($c == 1) {
	    	return TRUE;
	    }
	    return FALSE;
	}
	
}