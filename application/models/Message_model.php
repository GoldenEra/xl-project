<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends Base_Model {
    public function __construct() {

        $this->db_tablepre = 't_sys_';
        $this->table_name = 'message';
        parent::__construct();

        $this->load->model(array('Message_log_model'));
        $this->load->model(array('Address_model'));
        $this->load->model(array('User_model'));
        $this->load->model(array('Member_model'));
        $this->load->model(array('SMS_send_model'));
    }

   /**
    * 创建报警信息
    * @param  integer $repoter_user_id    [description]
    * @param  integer $repoter_address_id [description]
    * @param  string  $type               [description]
    * @param  string  $content            [description]
    * @param  string  $latitude           [description]
    * @param  string  $longitude          [description]
    * @param  string  $address_detail     [description]
    * @param  string  $image              [description]
    * @param  string  $video              [description]
    * @param  string  $thumb              [description]
    * @return [type]                      [description]
    */
   public function create($repoter_id = 0, $type = '', $content = '', 
                          $latitude = '', $longitude = '', $address_detail = '', 
                          $image = '',  $video = '', $thumb = '')
   {
      $insert_field = [];
      
      if (!empty($repoter_id)) {
        
        $insert_field['repoter_id'] = $repoter_id;
      }

      $user_info = $this->User_model->get_one(['user_id' => $repoter_id]);
       if (!$user_info) {
        
          return ['status' => -1, 'tips' => '该用户不存在', 'data' => []];
       }
      $repoter_address_id = $user_info['address_id'];
      $insert_field['repoter_address_id'] = $repoter_address_id;

      if (!empty($type)) {
        
        $insert_field['type'] = $type;
      }
      
      if (!empty($content)) {
        
        $insert_field['content'] = $content;
      }

      if (!empty($latitude)) {
        
        $insert_field['latitude'] = $latitude;
      }

      if (!empty($longitude)) {
        
        $insert_field['longitude'] = $longitude;
      }

      if (!empty($address_detail)) {
        
        $insert_field['address_detail'] = $address_detail;
      } else{

        // 报警分为三类，客户端报警，摄像头报警和机顶盒报警
        // 客户端报警的详细地址存储在address_detail中
        // 摄像头报警会传经纬度过来
        // 机顶盒报警的经纬度信息使用用户信息中的经纬度
        if (empty($longitude) || empty($latitude)) {
          $longitude = !empty($user_info['longitude']) ? $user_info['longitude'] : $longitude;
          $latitude = !empty($user_info['latitude']) ? $user_info['latitude'] : $latitude;
        }
        $insert_field['address_detail'] = $this->getAddressDetailByLL($longitude, $latitude);
      }

      if (!empty($image)) {
        
        $insert_field['image'] = $image;
      }

      if (!empty($video)) {
        
        $insert_field['video'] = $video;
      }

      if (!empty($thumb)) {
        
        $insert_field['thumb'] = $thumb;
      }
      $message_id = $this->insert($insert_field, true);
      // 添加日志
      if ($message_id) {
        # code...
        $message_log_id = $this->Message_log_model->addLog($message_id, 'CREATED', $repoter_id);
      }

      // 给所有同级管理员发送短信
      $admin_info = $this->Member_model->select(['address_id' => $repoter_address_id, 'is_admin' => 1]);
      if ($admin_info) {
          
          $mobile = array_column($admin_info, 'mobile');
          $a = $this->SMS_send_model->sendSMS($mobile);
      }


      return $message_id;
   }

   // 根据经纬度获取地址详情
   public function getAddressDetailByLL($longitude = 0, $latitude = 0) {
      $address_detail = '';
      if (!empty($latitude) && !empty($longitude)) {
        $url = 'http://restapi.amap.com/v3/geocode/regeo?key='.GAODE_KEY_ADDR.'&'.'location='.$longitude.','.$latitude;
        
        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($con, CURLOPT_HEADER, 0);
        curl_setopt($con, CURLOPT_POST, 1);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($con, CURLOPT_CONNECTTIMEOUT, 30);  

        $result = curl_exec($con);
        curl_close($con);
          // print_r($url);exit;
        if ($result) {
          $result = json_decode($result,true);
          if ($result['infocode'] == 10000) {

            $address_detail = $result['regeocode']['formatted_address'] ? : '';
          }
        }
      }

      return $address_detail;
   }

   /**
    * 后台管理员查看报警信息
    * 
    * @param  integer $user_id [description]
    * @param  integer $page    [description]
    * @param  integer $limit   [description]
    * @return [type]           [description]
    */
   public function listMessage($user_id = 0, $page_no = 1, $limit = 20, $show_page = 0)
   {

        if (empty($user_id)) {
              
              return ['status' => -1, 'tips' => '用户ID为空', 'data' => ''];
        }

        $user_info = $this->Member_model->get_one(array('user_id'=>$user_id));
        
        if (!$user_info) {
              
              return ['status' => -1, 'tips' => '用户不存在', 'data' => ''];
        }
        // if (!$user_info['is_admin']) {
        
        //     return ['status' => -1, 'tips' => '该用户不是管理员', 'data' => ''];
        // }
        
        // 消息是直接上报给该用户的上级地址的
        // 所以步骤
        // 1. 获取管理员的地址
        // 2. 获取管理员的一级子地址
        // 3. 消息管理中的地址等于1和2中获取的地址，则是该管理员所能看到的所有消息
        // 管理员地址
        
        /**
        $address_id = $user_info['address_id'];
        
        **/

        $level_one_address = array($user_info['address_id']);

        // 消息的地址就是管理员的一级子地址
        $level_one_address_info = $this->Address_model->get_one(['id' => $user_info['address_id']]);

        // 管理员的一级子地址, 只有一级子地址能看到下级信息,
        $level_one_address_all = $this->Address_model->select(['parent_id' => $user_info['address_id']]);

        $childIDs = [];
        if ($level_one_address_all) {
          
            $childIDs = array_column($level_one_address_all, 'id');
        }

        // 被删除的用户不能查看到报警信息
        $all_users = $this->Member_model->select([], 'user_id');
        $all_user_ids = [];
        if ($all_users) {
          $all_user_ids = array_column($all_users, 'user_id');
        }
        
        // 所有上级地址也能看到下级地址的信息
        // $childIDs = $this->Address_model->getChildIDs($user_info['address_id']);
        $address_ids = array_merge($level_one_address, $childIDs);
        $where = 'repoter_address_id in ('. implode(',', $address_ids).' )';
        if(!empty($all_user_ids)) {

          $where .= 'and repoter_id in ('.implode(',', $all_user_ids).' )';
        }

        $orderby = 'created desc';
        
        // $message_info = $this->Message_model->select(['repoter_address_id' => $address_id]);
  	$data_list = $this->Message_model->listinfo($where, '*', $orderby , $page_no, $limit, '', $limit, page_list_url('adminpanel/message/index',true));

       $user_message_total = $this->Message_model->count($where);
       
       $status_cn = ['CREATED' => '信息创建', 'PROCESS' => '信息处理中', 'FINISH' => '处理完毕'];
      $type_cn   = ['普通报警' ,'公安110','急救120','火警119'];

       $result = [];
       foreach ($data_list as $key => &$value) {
          
          $info = $this->User_model->get_one(['user_id' => $value['repoter_id']]);
          $value['status_cn'] = isset($status_cn[$value['status']]) 
                                ? $status_cn[$value['status']]
                                : '未知状态';
          $value['type_cn'] = isset($type_cn[$value['type']]) 
                                ? $type_cn[$value['type']]
                                : '普通报警';

          if ($info) {
            $value['user_info'] = $info;
          }else{
            $data_list[$key] = [];
            continue;
          }
       }
       // 移除空数组
       $result_arr = [];
       foreach ($data_list as $key => $value) {
         if (!empty($value)) {
            $result_arr[] = $value;
         }
       }
       
       if(empty($result_arr)) {
      
          return ['status' => -1, 'tips' => '没有找到报警信息', 'data' => []];
      }

      $result = ['list' => $result_arr, 'total' => $user_message_total];

       if ($show_page) {
         # code...
          $result['page_url'] = $this->Message_model->pages;
       }

      return ['status' => 200, 'tips' => '找到报警信息', 'data' => $result];
  }

   /**
    * 将信息上报给上级
    * 1. 信息地址
    * 2. 信息地址的上级地址
    * 3. 信息地址的上级地址是否和管理员地址相等，若想等，表示有处理权限，否则则无
    * 4. 更新reporter_id和address_detail
    * 
    * @param  integer $message_id [description]
    * @param  integer $user_id    [description]
    * @return [type]              [description]
    */
   public function toLeader($message_id = 0, $user_id = 0)
   {
       $admin_info = $this->User_model->get_one(['user_id' => $user_id/**, 'is_admin' => 1**/]);
       if (!$admin_info) {
        
          return ['status' => -1, 'tips' => '该用户不存在', 'data' => []];
       }

       $message_info = $this->get_one(['id' => $message_id]);
       if (!$message_info) {
            
            return ['status' => -1, 'tips' => '报警信息不存在', 'data' => []];
       }

       $address_id = $message_info['repoter_address_id'];
       $message_address_info = $this->Address_model->get_one(['id' => $address_id]);

       if (!$address_id || empty($message_address_info['parent_id'])) {
         
          return ['status' => -1, 'tips' => '该信息报警人没有上级，不能上报', 'data' => []];
       }

       // if($admin_info['address_id'] !== $address_id) {
       //
       //    return ['status' => -1, 'tips' => '您不能处理该信息', 'data' => []];
       // }

       // 信息未处理或者处理人不等于上报人
       if (!empty($message_info['receive_user_id']) &&
          $message_info['receive_user_id'] != $user_id) {

          return ['status' => -1, 'tips' => '该信息已经被处理，您不能处理', 'data' => []];
       }

       // 信息处理完毕后不能上报
       if ($message_info['status'] == 'FINISH') {
        
            return ['status' => -1, 'tips' => '信息处理完毕，不能上报', 'data' => []];
       }

       $repoter_address_id = $this->getParentInMember($address_id);
	     // $admin_address_info = $this->Address_model->get_one(['id' => $address_id]);
       
       $status = 'CREATED';
	     $level = $message_info['level'];
       $update_field = [
              'receive_user_id' => 0, 
		   				// 'repoter_id'	=> $user_id,
              'repoter_address_id' => $repoter_address_id, 
						  'status' => $status,
						  'level'	=> ($level + 1),
						  'modified'	=> date('Y-m-d H:i:s')];
	     // var_dump($update_field);exit;
       $this->update($update_field, 'id='.$message_id);

      // 给所有同级管理员发送短信
      $admin_info = $this->Member_model->select(['address_id' => $repoter_address_id, 'is_admin' => 1]);
      if ($admin_info) {
          
          $mobile = array_column($admin_info, 'mobile');
          $this->SMS_send_model->sendSMS($mobile);
      }

       
       $message_log_id = $this->Message_log_model->addLog($message_id, $status, $user_id);

       if($message_log_id) {
     
          $message_info = $this->get_one(['id' => $message_id]);
          return ['status' => 200, 'tips' => '更新成功', 'data' => $message_info];
      }

      return ['status' => -1, 'tips' => '更新失败', 'data' => $message_info];
   }


    // 查所有时间距离创建时间1小时，且状态未未处理的消息，将时间做成可配置的
    // 调用to leader 自动上报
    public function autoToLeader() {

        date_default_timezone_set('Asia/Shanghai');
		    $before = time() - CALL_POlICE_INTERVAL;
        // 一小时之前未被处理的消息

        $where = "status = 'CREATED' and receive_user_id=0 and unix_timestamp(modified) < ".$before. ' and unix_timestamp(created)<'.$before;
        $overOneHourMsg = $this->select($where);
        $successCount = 0;
        if ($overOneHourMsg) {
          
          foreach ($overOneHourMsg as $key => $value) {

             $address_info = $this->Address_model->get_one(['id' => $value['repoter_address_id']]);
            
             if (!empty($address_info['parent_id'])) {
               $message_id = $value['id'];
               $repoter_parent_address_id = $this->getParentInMember($value['repoter_address_id']);

                $this->update(['repoter_address_id' => $repoter_parent_address_id, 'modified' => date('Y-m-d H:i:s')], 'id='.$message_id);

				error_log(date('Y-m-d H:i:s', time())."  消息 $message_id 自动上报到地址".$address_info['parent_id']."成功\n\n", 3, LOG_PATH.'message.log');
			   
               $successCount++;
             }
          }
        }

      return ['status' => 200, 'tips' => "更新成功 $successCount 个", 'data' => []];
    }

    /**
     * 通过地址ID获取父ID，且这个ID在用户列表中必须存在
     * @param string $value [description]
     */
    private function getParentInMember($address_id = 0) {
      $parentIdInMember = 0;
      $parentIDs = $this->Address_model->getParentIDs($address_id);
      if (!empty($parentIDs)) {
        foreach ($parentIDs as $parentID) {
          $result = $this->Member_model->get_one(['address_id' => $parentID]);
          if ($result) {
            $parentIdInMember = $parentID;
            break;
          }
        }
      }

      return $parentIdInMember;
    }

   /**
    * 更新报警信息状态
    * @param  integer $message_id [信息ID]
    * @param  string  $status     [description]
    * @param  integer $user_id    [description]
    * @return [type]              [description]
    */
   public function updateStatus($message_id = 0, $status = '', $user_id = 0)
   {

      $user_info = $this->User_model->get_one(array('user_id' => $user_id/**, 'is_admin' => 1**/));
      if (!$user_info) {

        return ['status' => -1, 'tips' => '用户不存', 'data' => []];

      }
	   // var_dump($message_id);exit;
       $message_info = $this->get_one(['id' => $message_id]);
	   // var_dump($message_info);exit;
       if (!$message_info) {
            
            return ['status' => -1, 'tips' => '报警信息不存在', 'data' => []];
       }

	     $allMessageStatus = ['CREATED', 'PROCESS', 'FINISH'];
       if (!in_array($status, $allMessageStatus)) {
            
            return ['status' => -1, 'tips' => '报警状态不正确', 'data' => []];
       }

       if ($message_info['status'] == 'FINISH') {
        
          return ['status' => -1, 'tips' => '该报警信息已经结束，无需处理', 'data' => []];
       }
       // 报警状态校验

	   if($message_info['status'] == $status) {
	   
            return ['status' => -1, 'tips' => '信息正在被他人处理', 'data' => []];
	   }

	   // 如果报警信息已经被人处理了，则其他人不能处理该信息
       if ($message_info['status'] != 'CREATED' &&  $message_info['receive_user_id'] != $user_id) {
          
            return ['status' => -1, 'tips' => '该信息已被他人处理', 'data' => []];
       }

       $update_field = ['receive_user_id' => $user_id, 'status' => $status, 'modified' => date('Y-m-d H:i:s', time())];
	   // var_dump($update_field);exit;

       // 更新报警信息状态，并添加处理日志
       $this->update($update_field, 'id='.$message_id);

       $message_log_id = $this->Message_log_model->addLog($message_id, $status, $user_id);
	   if($message_log_id) {
	   
       		$message_info = $this->get_one(['id' => $message_id]);
			return ['status' => 200, 'tips' => '更新成功', 'data' => $message_info];
	   }

	   return ['status' => -1, 'tips' => '更新失败', 'data' => $message_info];
   }

   public function detail($message_id = 0)
   {
        $message_info = $this->get_one(array('id' => $message_id));
        if (!$message_info) {

            return ['status' => -1, 'tips' => '未找到报警信息', 'data' => $message_id];
        }

        $status_cn = ['CREATED' => '信息创建', 'PROCESS' => '信息处理中', 'FINISH' => '处理完毕'];

        $type_cn   = ['普通报警' ,'公安110','急救120','火警119'];
        
        $message_info['status_cn'] = isset($status_cn[$message_info['status']]) 
                                ? $status_cn[$message_info['status']]
                                : '未知状态';
        $message_info['type_cn'] = isset($type_cn[$message_info['type']]) 
                                ? $type_cn[$message_info['type']]
                                : '普通报警';

        $message_info['user_info'] = $this->User_model->get_one(array('user_id' => $message_info['repoter_id']));
      
      return ['status' => 200, 'tips' => '查找成功', 'data' => $message_info];

   }

   /**
    * 查看用户的报警信息
    * @param  integer $user_id [description]
    * @param  integer $page    [description]
    * @return [type]           [description]
    */
   public function myMessage($user_id =0, $page = 1)
   {
      $user_info = $this->User_model->get_one(array('user_id' => $user_id));
      if (!$user_info) {
        
        return ['status' => -1, 'tips' => '用户不存在', 'data' => ''];
      }
      // if ($user_info['is_admin'] == 1) {
        
      //   return ['status' => -1, 'tips' => '该用户是管理员，可以登录后台查看', 'data' => ''];
      // }

      $orderby = 'created desc';
      $user_message_list = $this->Message_log_model->listinfo(array('staff_id' => $user_id), '*', $orderby, $page, PAGE_SIZE, '', PAGE_SIZE);
      $user_message_total = $this->Message_log_model->count(array('staff_id' => $user_id));

      if (!$user_message_list) {
          
          return ['status' => -1, 'tips' => '该用户没有上报过任何信息', 'data' => ''];
      }

      $reuslt = [];
      $status_cn = ['CREATED' => '信息创建', 'PROCESS' => '信息处理中', 'FINISH' => '处理完毕'];
      
      foreach ($user_message_list as $key => $value) {
          $message_info = [];
          $message_info = $this->get_one(array('id' => $value['message_id']));
          if(!$message_info){
            continue;
          }
          $message_info['status_cn'] = isset($status_cn[$message_info['status']]) 
                                      ? $status_cn[$message_info['status']]
                                      : '未知状态';
          $message_info['user_info'] = $this->User_model->get_one(['user_id' => $message_info['repoter_id']]);

          $result[$value['message_id']] = $message_info;
      }
      $data = [];
      foreach ($result as $key => $value) {
        
        $data [] = $value;
      }

      $return_data = ['list' => $data, 'total' => $user_message_total];

      return ['status' => 200, 'tips' => '查找成功', 'data' => $return_data];

   }

  	public function generateRandomString($length = 10) {
  	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  	    $charactersLength = strlen($characters);
  	    $randomString = '';
  	    for ($i = 0; $i < $length; $i++) {
  		        $randomString .= $characters[rand(0, $charactersLength - 1)];
  		    }
  		return $randomString;
  	}
   
}
