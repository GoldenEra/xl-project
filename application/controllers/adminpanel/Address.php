<?php
/*
*    地址管理模块
*/
class Address extends Admin_Controller {
    
    public function __construct()
    {
        parent::__construct();

        //请在这里初始化模块相关设置
        $this->load->library('tree');
        $this->load->helper(array('auto_codeIgniter'));
        $this->load->model(array('Address_model'));
    }

    /*
    * 这里是默认的首页，显示地址列表
    */
    public function index(){
        
        
        $treeModel = $this->tree;
        $treeModel->icon = array('&nbsp;&nbsp;&nbsp;│ ','&nbsp;&nbsp;&nbsp;├─ ','&nbsp;&nbsp;&nbsp;└─ ');
        $treeModel->nbsp = '&nbsp;&nbsp;&nbsp;';
        $address_list = $this->Address_model->getAddressList();

        $level_arr = ['中国', '省', '市', '区/县', '镇/乡', '村'];
        
        $result = [];
        foreach($address_list as $address) {

            $address['str_manage'] = '';
            $address['zipcode'] = empty($address['zipcode']) ? '0000' : $address['zipcode'];
            $address['level_name'] = $level_arr[$address['level']];
            $address['str_manage'] =aci_ui_a($this->page_data['folder_name'],$this->page_data['controller_name'],'addPage',$address['id'],' class="btn btn-default btn-xs"','<span class="glyphicon glyphicon-plus"></span> 新增地址',true);
            $address['str_manage'] .= " ". aci_ui_a($this->page_data['folder_name'],$this->page_data['controller_name'],'edit',$address['id'],' class="btn btn-default btn-xs"','<span class="glyphicon glyphicon-wrench"></span> 修改地址',true);
            
            $result[] = $address;
        }
    
        $str  = "<tr>
                    <td><input type='checkbox' name='pid[]' value='\$id' /></td>
                    <td>\$spacer\$name</td>
                    <td>\$level_name</td>
                    <td>\$zipcode</td>
                    <td>\$str_manage</td>
                </tr>";

        $treeModel->init($result);

        // var_dump($result);exit;
        $table_html = $treeModel->get_tree(0, $str);
        
        $this->view('index',array('require_js'=>true,'table_html'=>$table_html));
    }

    public function indexTree()
    {
        $tree = '';
        $address_list = $this->Address_model->getAddressList();
        $html = $this->Address_model->toTree($address_list);


        $this->view('indextree',array('require_js'=>true, 'html' => $html));
    }

 
    

    public function edit($id = 0)
    {
        $id = intval($id);
        if (!isset($id) || empty($id)) {
            $this->showmessage('未找到该地址', ADMIN_URL_PATH);
        }
        $current_address = $this->Address_model->get_one('id='.$id);

        $address_list = $this->Address_model->getAddressList();
        
        $select_categorys = $this->Address_model->formateAddressTree($address_list, $current_address['parent_id']);
        
        $is_add = false;

        $treeHtml = $this->Address_model->toTree($address_list);

        return $this->view('add', array('require_js'=>true, 'select_categorys'=>$select_categorys,'is_add'=>$is_add, 'current_address' => $current_address, 'treeHtml' => $treeHtml));
    }

    public function addPage($id = 0)
    {
        $id =  intval($id);
        $is_add = true;

        $address_list = $this->Address_model->getAddressList();
        $select_categorys = $this->Address_model->formateAddressTree($address_list, $id);

        $treeHtml = $this->Address_model->toTree($address_list);

        return $this->view('add',array('require_js'=>true,'select_categorys'=>$select_categorys,'is_add'=>$is_add, 'treeHtml' => $treeHtml));
    }


    /**
     * 添加地址
     * 
     */
    function add(){

        $_POST['parent_id'] = $_POST['parent_id_tree'];

        if (!isset($_POST['parent_id']) || !is_ajax()) {
            
            $this->showmessage('提交数据错误', site_url('adminpanel/address/indexTree'));
        }
        // 用作地址更新
        $id = intval($this->input->post('address_id'));
        
        // 保存
        $parent_id = intval($this->input->post('parent_id'));
        $name = trim($this->input->post('address_name',''));
        $zipcode = trim($this->input->post('zipcode',''));

        if(empty($id) && $this->Address_model->isNameExist($name)){

            $this->showmessage('地址已经存在', site_url('adminpanel/address/indexTree'));

            // echo json_encode(array('status'=>false,'tips'=>'地址已经存在')); exit;
        }

        $params = ['parent_id' => $parent_id, 'name' => $name, 'zipcode' => $zipcode];
        
        if (isset($id) && !empty($id)) {
            $this->showmessage('不支持地址更新操作', site_url('adminpanel/address/indexTree'));
            
            // $result = $this->Address_model->update_address($id, $params);
        } else {

            $result = $this->Address_model->add($params);
        }
        
        if($result['status']){
            $this->showmessage('操作成功', site_url('adminpanel/address/indexTree'));
            // echo json_encode(array('status'=>true,'tips'=>"ok")); exit;
        }
        else{
            $this->showmessage('新增地址失败', site_url('adminpanel/address/indexTree'));
            // echo json_encode(array('status'=>false,'tips'=>"更新/新增地址失败")); exit;
        }
    }


    public function delete($id = 0)
    {
        // $address_ids = $this->input->post('pid');
        $address_ids = intval($id);
        $result = $this->Address_model->delete_address($address_ids);
        if($result['status'])
        {
            $this->showmessage('操作成功', site_url('adminpanel/address/indexTree'));
            // echo json_encode(['status' => true, 'tips' => '删除成功']);
            
        }else 
        {
            $this->showmessage('操作失败');
            // echo json_encode(['status' => falst, 'tips' => '删除失败']);
        }
    }


}
