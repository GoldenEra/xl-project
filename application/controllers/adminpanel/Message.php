<?php
/*
*    报警信息
*/
class Message extends Admin_Controller {
    
    function __construct()
    {
        parent::__construct();
        //请在这里初始化模块相关设置

        # 这里加载Demo模型，稍修建立，如果不需要用到数据库，请注释掉这行
        $this->load->model(array('Message_model'));
    }

    /*
    * 这里是默认的首页，显示当前用户收到的报警信息
    */
    public function index($page_no = 1){

        $user_id = $this->user_id;

        $page_no = intval($page_no);

        $data_list = $this->Message_model->listMessage($user_id, $page_no, 20, 1);
        // var_dump($this->Message_model);exit;

        if($data_list['status'] == 200) {

            $data = $data_list['data'];
            $this->view('index', array('data' => $data, 'require_js' => true));
        } else{

            $this->view('index', array('data' => [], 'require_js' => true));
        }

    }

    public function detail($message_id = 0)
    {
        $message_id = intval($message_id);
        $result = $this->Message_model->detail($message_id);
        if ($result['status'] != 200) {
            
            $this->showmessage($result['tips'], site_url('adminpanel/message/index'));
        }

        $this->view('detail', array('data' => $result['data'], 'require_js' => true));
    }



    public function toLeader($message_id = '')
    {
        $result = $this->Message_model->toLeader($message_id, $this->user_id);
		return $this->showmessage($result['tips'], $_SERVER['HTTP_REFERER']);
    }


    /**
     * 信息处理
     * @param  integer $message_id [description]
     * @param  string  $status     [description]
     * @return [type]              [description]
     */
    public function updateMessageStatus($message_id = 0, $status = '')
    {
        $user_id = $this->user_id;
        
        $result = $this->Message_model->updateStatus($message_id, $status, $user_id);

        $this->showmessage($result['tips'], $_SERVER['HTTP_REFERER']);
    }

    public function autoToLeader()
    {
        // 查处所有时间距离创建时间1小时，且状态未未处理的消息，将时间做成可配置的
        // 调用to leader 自动上报
        // 
    }
}
