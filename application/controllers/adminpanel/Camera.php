<?php
/*
*    这是一个摄像头模块
*/
class Camera extends Admin_Controller {
    
    public function __construct()
    {
        parent::__construct();
        //请在这里初始化模块相关设置

        # 这里加载Demo模型，稍修建立，如果不需要用到数据库，请注释掉这行
        $this->load->model(array('Camera_model'));
        $this->load->model(array('Address_model'));
        $this->load->model(array('Member_role_model'));
        $this->load->model(array('Role_camera_model'));

    }

    /*
    * 这里是默认的首页，访问
    */
    public function index($page_no = 1){

        $count = 50;

        $group_id = $this->group_id;

        $where = 'role_id='.$group_id;
        $orderby = 'created';
        $page_no = max(intval($page_no),1);

        $data_list = $this->Role_camera_model->listinfo($where, '*',$orderby , $page_no, $count, '',$count, page_list_url('adminpanel/camera/index/', true));

        // $camera_list = $this->Camera_model->getCameraList($page, $count, $group_id);
        // var_dump($camera_list);exit;
        $provider_arr = ['DAHUA' => '大华', 'YUSHI' => '宇视', 'HAIKANG' => '海康'];
        foreach ($data_list as &$camera) {
            
            $camera_info = $this->Camera_model->get_one(['id' => $camera['camera_id']]);
            if (!$camera_info) {
				$camera = [];
                continue;
            }
            $provider = empty($camera_info['provider']) ? 'DAHUA' : $camera_info['provider'];
            $camera['provider_cn'] = isset($provider_arr[$provider])
                                     ? $provider_arr[$provider]
                                     : '未知类型';
            $camera = array_merge($camera, $camera_info);
            $camera['address_detail'] = $this->Address_model->getAddressDetailByID($camera['address_id']);
        }        
		// 索引重建
		// var_dump($data_list);exit;
		$data_list = array_filter($data_list);

        $this->view('index', array('require_js'=>true, 'camera_list' => $data_list, 'pages'=>$this->Role_camera_model->pages));
    }

    public function messageMap($type = 'ALL')
    {
        $user_id = $this->user_id;

        $page_no = 1;
        $number = MAX_PAGE_SIZE;

        $data_list = $this->Message_model->listMessage($user_id, $page_no, MAX_PAGE_SIZE, 1);

	$messages = [];
        if($data_list['status'] == 200) {

		$messages = $data_list['data']['list'];
		foreach ($messages as $key => $message) {
		    if ($message['status'] == 'FINISH' || empty($message['longitude']) || $message['longitude'] == 'null') {
					$messages[$key] = [];
			continue;
		    }

		    // 下面的数据是为了地图显示的
		    $messages[$key]['center'] = [floatval($message['longitude']), floatval($message['latitude'])];
		    $messages[$key]['name'] = $message['content'];
		    $messages[$key]['show_type'] = 'message';
		}
		$messages = array_filter($messages);
		//var_dump($messages);exit;

        }

        
        // 获取摄像头信息
        $cameras = [];
        $cameraMap = $this->Camera_model->cameraMap();
		// var_dump($cameraMap);exit;
        if ($cameraMap['status'] == 200) {
            $cameras = $cameraMap['data'];
			foreach($cameras as &$camera) {
				if(!isset($camera['longitude']) || empty($camera['longitude']) || $camera['longitude'] == 'null') {
					$camera = [];
                    continue;
				}
			}
			$cameras = array_filter($cameras);
        }
		// var_dump($cameras);exit;

        // 获取用户信息
        $users = [];
        $userInfo = $this->User_model->select('', '*');

        if ($userInfo) {
            foreach($userInfo as &$user) {
                if(empty($user['longitude'])|| empty($user['latitude'])) {
                    $user = [];
                    continue;
                }

                // 下面的数据是为了地图显示的
                $user['center'] = [floatval($user['longitude']), floatval($user['latitude'])];
                $user['name'] = $user['username'];
                $user['show_type'] = 'user';
            }

            $users = array_filter($userInfo);
        }
        // var_dump($users);exit;

        $result = [];
        if ($type == 'camera') {
            $result = $cameras;
        }else if ($type == 'message') {
            $result = $messages;
        }else if ($type == 'user') {
            $result = $users;
        }
        else{
            $result = array_merge($messages, $cameras, $users);
        }

        // 过滤掉经纬度为空的记录
		$returnResult = [];
        foreach ($result as $value) {
            if (!isset($value['longitude']) || 
                !isset($value['latitude'])  ||
                empty($value['longitude'])  ||
                empty($value['latitude'])) {
				continue;
            }
			$returnResult[] = $value;
        }

        // 权限检查
        $toLeaderPriv = $updateMessageStatusPriv = 0;
        $toLeader = $this->User_model->checkPriv($this->user_id, 'toLeader');
        if($toLeader['status'] == 200) {
            $toLeaderPriv = 1;
        }
        $updateMessageStatus = $this->User_model->checkPriv($this->user_id, 'updateMessageStatus');
        if($updateMessageStatus['status'] == 200) {
            $updateMessageStatusPriv = 1;
        }

        $this->view('cameramap', array('camera_list' => $returnResult, 'require_js' => true, 'toLeaderPriv' => $toLeaderPriv, 'updateMessageStatusPriv' => $updateMessageStatusPriv));
    }

    /**
     * 增加一个摄像头，如果传过来ID，说明是更改
     * @param integer $id [description]
     */
    public function addPage()
    {
        $camera = $this->Camera_model->default_info();
        $address_list = $this->Address_model->getAddressList();
        $select_categorys = $this->Address_model->formateAddressTree($address_list);
        $this->view('edit', array('require_js' => true, 'is_edit' => false, 'camera' => $camera, 'select_categorys' => $select_categorys));
    }

    public function add()
    {
        // 保存
        $camera_id = $this->input->post('camera_id');
        $camera_name = trim($this->input->post('camera_name'));
        $provider = trim($this->input->post('provider'));
        // $camera_type = trim($this->input->post('camera_type'));

        if (!in_array($provider, ['DAHUA', 'HAIKANG', 'YUSHI'])) {

            return $this->showmessage('摄像头类型不正确', site_url('adminpanel/camera/index'));
        }
        $custom_id = $this->input->post('custom_id');
        $address_id = intval($this->input->post('address_id'));

        if (!empty($camera_id)) {
            $params = ['camera_name' => $camera_name, 'provider' => $provider,
                        'custom_id' => $custom_id, 'address_id' => $address_id, 
                        /**'camera_type' => $camera_type**/];

            $result = $this->Camera_model->updateCamera($camera_id, $params);
        } else {

            $result = $this->Camera_model->add($camera_name, $custom_id, $address_id, $provider/**$camera_type**/);
        }
        
        echo json_encode(array('status'=>true,'tips'=>"ok")); exit;
    }

    public function editPage($id = 0)
    {
        $camera = $this->Camera_model->get_one('id='.$id);
        $address_id = $camera['address_id'];

        $address_list = $this->Address_model->getAddressList();
        $select_categorys = $this->Address_model->formateAddressTree($address_list, $address_id);

        $this->view('edit', array('require_js' => true, 'is_edit' => true, 'camera' => $camera, 'select_categorys' => $select_categorys));
    }

    public function update()
    {
        $id          = intval($this->input->post('camera_id'));
        $camera_name = trim($this->input->post('camera_name'));
        $provider = trim($this->input->post('provider'));
        if (!in_array($provider, ['DAHUA', 'HAIKANG', 'YUSHI'])) {

            return $this->showmessage('摄像头类型不正确', site_url('adminpanel/camera/index'));
        }

        $custom_id   = trim($this->input->post('custom_id'));
        $address_id  = intval($this->input->post('address_id'));

        $params = ['camera_name' => $camera_name, 'custom_id' => $custom_id, 'address_id' => $address_id, 'provider' => $provider];

        $result = $this->Camera_model->updateCamera($id, $params);
        
        if (!$result) {
            
            echo json_encode(array('status' => false,'tips'=>"更新相机信息失败")); exit;
        }

        echo json_encode(array('status' => true, 'tips' => "ok")); exit;
    }

    

    public function delete()
    {
        $ids = $this->input->post('pid');
        $result = $this->Camera_model->delete_camera($ids);
        if (!$result) {

            return $this->showmessage('删除相机信息失败', site_url('adminpanel/camera/index'));
        }

        return $this->showmessage('删除相机信息成功', site_url('adminpanel/camera/index'));

    }

}
