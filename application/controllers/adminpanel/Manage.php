<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends Admin_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Times_model'));
		$this->load->model(array('User_model'));
		$this->load->model(array('User_model'));
		$this->load->model(array('Captcha_model'));
		$this->load->model(array('Member_role_priv_model'));
	}

	function cache(){
		$this->reload_all_cache();
		$this->showmessage('全局缓存成功',site_url('adminpanel'));
	}

	function go($id=0){
		if($id==0) exit() ;

		if(isset($this->current_role_priv_arr[$id])){
			$arr = $this->cache_module_menu_arr[$id];
			if(!isset($arr))exit();
			$arr_parentid = explode(",",$arr['arr_parentid']);

			if(count($arr_parentid)>2) redirect( base_url($arr['folder'].'/'.$arr['controller'].'/'.$arr['method']));
			else{
				foreach($this->cache_module_menu_arr as $k=>$v){
					if($v['parent_id']==$id){

						if(isset($this->current_role_priv_arr[$v['menu_id']])){
							redirect(base_url($v['folder'].'/'.$v['controller'].'/'.$v['method']));
							break;
						}
						
					}
				}
			}
		}
		exit();
	}
	
	function index()
	{
		$homeShow = array(
		    array(  'name' => '地址管理', 
		            'controller' => 'address', 
		            'method' => 'indexTree', 
		            'url' => '/adminpanel/address/indexTree', 
		            'icon' => 'gg'),

		    array(  'name' => '用户管理',
		            'controller' => 'role',
		            'method' => 'index',
		            'url' => '/adminpanel/role/index',
		            'icon' => 'users'),

		    array(  'name' => '摄像头管理',
		            'controller' => 'camera',
		            'method' => 'index',
		            'url' => '/adminpanel/camera/index',
		            'icon' => 'video-camera'),

		    array(  'name' => '报警信息管理',
		            'controller' => 'message',
		            'method' => 'index',
		            'url' => '/adminpanel/message/index',
		            'icon' => 'info'),

		    array(  'name' => ' 资料分类管理',
		            'controller' => 'materialType',
		            'method' => 'index',
		            'url' => '/adminpanel/materialType/index',
		            'icon' => 'database'),

		    array(  'name' => '资料管理',
		            'controller' => 'material',
		            'method' => 'index',
		            'url' => '/adminpanel/material/index',
		            'icon' => 'medium'),

		    array(  'name' => '地理信息',
		            'controller' => 'camera',
		            'method' => 'messageMap',
		            'url' => '/adminpanel/camera/messageMap',
		            'icon' => 'map-o'),

		);
		$role_id = $this->group_id;
		$resultShow = [];
		if ($role_id === SUPERADMIN_GROUP_ID) {
			$resultShow = $homeShow;
		}else{
			foreach ($homeShow as $key => $value) {
				$result = $this->Member_role_priv_model->get_one(array('role_id' => $role_id, 'controller' => $value['controller'], 'method' => $value['method']));
					if ($result) {
					$resultShow[] = $value;
					}
				}
		}
		
		$this->view('index',array('require_js'=>true, 'show' => $resultShow));
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('adminpanel'));
	}
	
	function login()
	{

		$this->reload_all_cache();
		
		$username = $this->input->post('username', TRUE);
		$cap = $this->input->post('cap', TRUE);
		
		if(isset($username)) {

			$captcha = $this->session->userdata('captcha');
			$captcha = trim($captcha);
			$captcha = strtolower($captcha);

			$cap = trim($cap);
			$cap = strtolower($cap);

			$newCapResult = $this->Captcha_model->generate_captcha();
			$newCapUrl = '';
			if ($newCapResult['status'] == 200) {
				$newCapUrl = $newCapResult['data'];
			}

			if ($captcha !== $cap ) {
				exit(json_encode(array('status'=>false,'tips'=>'验证码不正确', 'newCap' => $newCapUrl)));
			}
			$username = isset($username) ? trim($username) : exit(json_encode(array('status'=>false,'tips'=>'手机号不能为空', 'newCap' => $newCapUrl)));
			if($username=="")exit(json_encode(array('status'=>false,'tips'=>'手机号不能为空', 'newCap' => $newCapUrl)));
			$this->load->model('Times_model');
			
			//密码错误剩余重试次数
			// $rtime = $this->Times_model->get_one(array('username'=>$username,'is_admin'=>1));
			// $maxloginfailedtimes = 5;
			// if($rtime)
			// {
			// 	if($rtime['failure_times'] > $maxloginfailedtimes) {
			// 		$minute = 60-floor((SYS_TIME-$rtime['logintime'])/60);
			// 		exit(json_encode(array('status'=>false,'tips'=>' 密码尝试次数过多，被锁定一个小时')));
			// 	}
			// }

			//查询帐号，默认组1为超级管理员
			$r = $this->Member_model->get_one(array('mobile'=>$username));
			if(!$r) exit(json_encode(array('status'=>false,'tips'=>' 手机号或密码不正确', 'newCap' => $newCapUrl)));
			$pass = $this->input->post('password', TRUE);
			$password = md5(md5(trim($pass).$r['encrypt']));

			$ip = $this->input->ip_address();
			if($r['password'] != $password) {
				exit(json_encode(array('status'=>false,'tips'=>' 手机号或密码不正确', 'newCap' => $newCapUrl)));
			}
			// if($r['password'] != $password) {
			// 	if($rtime && $rtime['failure_times'] < $maxloginfailedtimes) {
			// 		$times = $maxloginfailedtimes-intval($rtime['failure_times']);
			// 		$this->Times_model->update(array('login_ip'=>$ip,'is_admin'=>1,'failure_times'=>' +1'),array('username'=>$username));
			// 	} else {
			// 		$this->Times_model->delete(array('username'=>$username,'is_admin'=>1));
			// 		$this->Times_model->insert(array('username'=>$username,'login_ip'=>$ip,'is_admin'=>1,'login_time'=>SYS_TIME,'failure_times'=>1));
			// 		$times = $maxloginfailedtimes;
			// 	}

			// 	// exit(json_encode(array('status'=>false,'tips'=>' 密码错误您还有'.$times.'机会')));
			// }

			// 不是后台管理员，不能登陆后台
			if (empty($r['is_admin'])) {
				exit(json_encode(array('status'=>false,'tips'=>'您不是后台管理用户，请向管理员申请', 'newCap' => $newCapUrl)));
			}

			$this->Times_model->delete(array('username'=>$username));
			// if($r['is_lock'])exit(json_encode(array('status'=>false,'tips'=>' 您的帐号已被锁定，暂时无法登录')));
			$this->Member_model->update(array('last_login_ip'=>$ip,'last_login_time'=>date('Y-m-d H:i:s')),array('user_id'=>$r['user_id']));

			$this->session->set_userdata('user_id',$r['user_id']);
			$this->session->set_userdata('user_fullname',$r['fullname']);
			$this->session->set_userdata('user_name',$username);
			$this->session->set_userdata('group_id',$r['group_id']);
			$this->session->set_userdata('is_admin',$r['is_admin']);

			exit(json_encode(array('status'=>true,'tips'=>' 登录成功','next_url'=>site_url($this->page_data['folder_name']))));

		}else {

			$capResult = $this->Captcha_model->generate_captcha();
			$filename = '';
			if ($capResult['status'] == 200) {
				$filename = $capResult['data'];
			}
			$this->admin_tpl('login', array('require_js'=>true, 'filename' => $filename));
		}
	}

}
