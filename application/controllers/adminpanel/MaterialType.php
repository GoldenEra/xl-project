<?php
/*
*    这是一个材料的分类管理
*/
class MaterialType extends Admin_Controller {
    
    public function __construct()
    {
        parent::__construct();
        //请在这里初始化模块相关设置

        # 这里加载Demo模型，稍修建立，如果不需要用到数据库，请注释掉这行
        $this->load->model(array('Material_type_model'));
        $this->load->model(array('User_model'));

    }

    /*
    * 这里是默认首页
    */
    public function index($page_no = 1){

        $page_no = max(intval($page_no),1);
        $orderby = 'created';
        $where = '';

        $material_type_list = $this->Material_type_model->getMaterialTypeList();

        $treeModel = $this->tree;
        $treeModel->icon = array('&nbsp;&nbsp;&nbsp;│ ','&nbsp;&nbsp;&nbsp;├─ ','&nbsp;&nbsp;&nbsp;└─ ');
        $treeModel->nbsp = '&nbsp;&nbsp;&nbsp;';

        $result = [];
        foreach($material_type_list as $material_type) {

            $material_type['str_manage'] = aci_ui_a($this->page_data['folder_name'],$this->page_data['controller_name'],'addPage',$material_type['id'],' class="btn btn-default btn-xs"','<span class="glyphicon glyphicon-plus"></span> 新增分类',true);
            $material_type['str_manage'] .= " ". aci_ui_a($this->page_data['folder_name'],$this->page_data['controller_name'],'edit',$material_type['id'],' class="btn btn-default btn-xs"','<span class="glyphicon glyphicon-wrench"></span> 修改分类',true);
            
            $result[] = $material_type;
        }
    
        $str  = "<tr>
                    <td><input type='checkbox' name='pid[]' value='\$id' /></td>
                    <td>\$spacer\$name</td>
                    <td>\$str_manage</td>
                </tr>";

        $treeModel->init($result);

        $table_html = $treeModel->get_tree(0, $str);
        
        $this->view('index', array('require_js'=>true, 'table_html' => $table_html));
    }

    /**
     * 添加分类页面
     * @param integer $id [description]
     */
    public function addPage($id = 0)
    {
        $id =  intval($id);
        $is_add = false;
        if (empty($id)) {
            $is_add = true;
        }

        $materialType_list = $this->Material_type_model->getmaterialTypeList();
        $select_categorys = $this->Material_type_model->formatematerialTypeTree($materialType_list, $id);

        return $this->view('add',array('require_js'=>true,'select_categorys'=>$select_categorys,'is_add' => $is_add));
    }

    /**
     * 保存材料分类
     */
    public function add()
    {
        // 用作类型更新
        $id          = intval($this->input->post('material_type_id', TRUE));

        // 保存
        $parent_id = intval($this->input->post('type_id', TRUE));
        $name = trim($this->input->post('material_type_name', TRUE));
        $name = $this->db->escape_str($name);

        if(empty($id) && $this->Material_type_model->isNameExist($name)){

            $this->showmessage('该分类名已经存在', site_url('adminpanel/materialType/index'));
        }


        $uploads_dir = UPLOAD_PATH.'image'.DIRECTORY_SEPARATOR;
        // $uploads_dir = '/var/www/ACI/uploadfile/image/';
        $config['upload_path'] = $uploads_dir;
        $config['max_size'] = '10240';
       	$config['allowed_types'] = 'jpg|jpeg|png';

        $this->load->library('upload', $config);
        $files = $_FILES;
        
        if (isset($files['material_type_icon']) && !empty($files['material_type_icon'])) {
			
            // 过滤文件名字
            $file_name = $this->User_model->normalizeString($files['material_type_icon']['name']);
            $file_name = mb_substr($file_name, -60);
            $_FILES['material_type_icon']['name']= $file_name;
            if (!$this->upload->do_upload('material_type_icon'))
            {
                 
                $error = array('error' => $this->upload->display_errors());
				// print_r($error);exit;
                $this->showmessage('图标上传失败，请重新选择', site_url('adminpanel/materialType/index'));
            } 
            else
            {
                $upload_data = $this->upload->data();
            }
        }
        $params = ['name' => $name, 'parent_id' => $parent_id];

        if(isset($upload_data['raw_name'])) {

            $params['icon'] = 'image'.DIRECTORY_SEPARATOR.$upload_data['raw_name'].$upload_data['file_ext'];
        }

        if (isset($id) && !empty($id)) {
            
            $result = $this->Material_type_model->updateMaterialType($id, $params);
        } else {

            $result = $this->Material_type_model->add($params);
        }
        // var_dump($result);exit;
        
        if($result['status'] == 200){
            $this->showmessage('添加/修改分类成功', site_url('adminpanel/materialType/index'));
        }
        else{
            $this->showmessage($result['tips'], site_url('adminpanel/materialType/index'));
        }
    }

    public function edit($id = 0)
    {
        $materialType = $this->Material_type_model->get_one('id='.$id);
        
        $parent_id = $materialType['parent_id'];

        $materialType_list = $this->Material_type_model->getmaterialTypeList();
        $select_categorys = $this->Material_type_model->formatematerialTypeTree($materialType_list, $parent_id);

        $this->view('add', array('require_js' => true, 'is_add' => false, 'materialType' => $materialType, 'current_material' => $materialType, 'select_categorys' => $select_categorys));
    }

    public function update()
    {
        $id          = intval($this->input->post('material_type_id', TRUE));
        $parent_id   = intval($this->input->post('parent_id', TRUE));
        $material_type_name = trim($this->input->post('material_type_name', TRUE));
        $material_type_name = $this->db->escape_str($material_type_name);

        $params = ['name' => $material_type_name, 'parent_id' => $parent_id];

        $result = $this->Material_type_model->updateMaterialType($id, $params);
        
        if ($result['status'] == 200) {
            
            echo json_encode(array('status' => false,'tips'=>"更新分类信息失败")); exit;
        }

        echo json_encode(array('status' => true, 'tips' => "ok")); exit;
    }

    

    public function delete()
    {
        $ids = $this->input->post('pid', TRUE);
        $result = $this->Material_type_model->delete_material_type($ids);
        if ($result['status'] != 200) {

            return $this->showmessage('删除分类信息失败', site_url('adminpanel/materialType/index'));
        }

        return $this->showmessage('删除分类信息成功', site_url('adminpanel/materialType/index'));

    }

}
