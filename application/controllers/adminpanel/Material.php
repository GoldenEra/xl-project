<?php
/*
*    这是一个摄像头模块
*/
class Material extends Admin_Controller {
    
    public function __construct()
    {
        parent::__construct();
        //请在这里初始化模块相关设置

        # 这里加载Demo模型，稍修建立，如果不需要用到数据库，请注释掉这行
        $this->load->model(array('Material_type_model'));
        $this->load->model(array('Material_model'));
        $this->load->model(array('User_model'));

    }


    /*
    * 这里是默认首页
    */
    public function index($page_no = 1){

        $page_no = max(intval($page_no),1);
        $orderby = 'created';
        $where   = '';

        $data_list = $this->Material_model->listinfo($where, '*',$orderby , $page_no, PAGE_SIZE, '', PAGE_SIZE, page_list_url('adminpanel/material/index', true));

        $type_cn = ['image' => '图片', 'video' => '视频', 'text' => '文本', 'animation' => '动画'];
        // 加入分类信息
        foreach ($data_list as &$material) {
            
			$type_info = $this->Material_type_model->get_one(['id' => $material['material_type_id']]);
			if(!$type_info){
				$material = [];
				continue;
			}
            $material['type_info'] = $type_info;
            $material['type_cn'] = isset($type_cn[$material['type']])
                                    ? $type_cn[$material['type']]
                                    : '未知类型';
            // var_dump($material);exit;
        }        
		$data_list = array_filter($data_list);

        $this->view('index',array('require_js'=>true, 'material_list' => $data_list, 'pages'=>$this->Material_model->pages));
    }


    /**
     * 增加一个资料，如果传过来ID，说明是更改
     * @param integer $id [description]
     */
    public function addPage()
    {
        $material = $this->Material_model->default_info();
        $material_type = $this->Material_type_model->getMaterialTypeList();
        $select_categorys = $this->Material_model->formateMaterialTree($material_type);

        $this->view('edit', array('require_js' => true, 'is_edit' => false, 'material' => $material, 'select_categorys' => $select_categorys));
    }

    /**
     * 保存地址
     */
    public function add()
    {
        if (!isset($_POST['type_id'])) {
            
            $this->showmessage('请先选择分类', site_url('adminpanel/material/index'));
        }

        // 用作地址更新
        $id = intval($this->input->post('material_id'));
        $type_id = intval($this->input->post('type_id'));
        $type = $this->input->post('type');
        $content = $this->input->post('ueditor-content');
        $material_name = trim($this->input->post('material_name'));

        $isLevelTwo = $this->Material_type_model->isLevelTwo($type_id);
        if ($isLevelTwo['status'] != 200) {
            $this->showmessage('只能为二级材料添加材料，请重新选择分类', site_url('adminpanel/material/index'));
        }
        $successCount = 0;

        if ($type == 'text') {

            $params['material_type_id'] = $type_id;
            $params ['type'] = $type;
            $params ['suffix'] = '.html';
            $params ['path'] = '';
            $params ['content'] = $content;
            $params ['name'] = $material_name;

            // print_r($params);exit;
            if (isset($id) && !empty($id)) {
            
                $result = $this->Material_model->update_material($id, $params);
            } else {

                $result = $this->Material_model->add($params);
            }

            if($result['status'] == 200){
                    $successCount++;
            }

        } else if ($type == 'other'){
            $files = $_FILES;
            $cpt = count($files['files']['name']);
            for ($i=0; $i < $cpt; $i++) { 
                // 过滤文件名
                $file_name = $this->User_model->normalizeString($files['files']['name'][$i]);
                $file_name = mb_substr($file_name, -60);
                if (empty($file_name)) {
                    
                    continue;
                }

                $_FILES['files']['name']= $file_name;
                $_FILES['files']['type']= $files['files']['type'][$i];
                $_FILES['files']['tmp_name']= $files['files']['tmp_name'][$i];
                $_FILES['files']['error']= $files['files']['error'][$i];
                $_FILES['files']['size']= $files['files']['size'][$i]; 
                if (strpos($_FILES['files']['type'], 'image') !== false) {
                    $type = 'image';
                }else{
                    $type = 'video';
                }

                $config['upload_path'] = UPLOAD_PATH.$type.DIRECTORY_SEPARATOR;
                $config['max_size'] = UPLOAD_SIZE;
                $config['allowed_types'] = 'jpg|jpeg|png|tif|mp4|rmvb';
                // $config['allowed_types'] = ['image/jpg', 'image/jpeg', 'image/png', 'video/tif', 'video/mp4', 'video/rmvb'];
                $this->load->library('upload', $config);

                $upload_data = '';
                $params = [];

                // var_dump($_FILES['files']);exit;
                if (!$this->upload->do_upload('files'))
                {
                     
                    $error = array('error' => $this->upload->display_errors());
                    // print_r($error);exit;
                    $this->showmessage('上传资料失败', site_url('adminpanel/material/index'));
                    // $this->showmessage('上传图片失败', site_url('adminpanel/material/index'));
                } 
                else
                {
                    $upload_data = $this->upload->data();
                }

                $params = $this->buildUploadParams($upload_data, $type);
                $params['material_type_id'] = $type_id;
                $params['type'] = $type;
                $params['content'] = '';
                $thumb = '';
                // 如果是视频，生成缩略图
                if ($type == 'video') {
                    $thumbResult = $this->Material_model->getVideoThumb(UPLOAD_PATH.$params['path']);

                    if ($thumbResult['status'] == 200) {
                        $thumb = $thumbResult['data'];
                    }
                }
                
                $params['thumb'] = $thumb;
                if (isset($id) && !empty($id)) {
                
                    $result = $this->Material_model->update_material($id, $params);
                } else {

                    $result = $this->Material_model->add($params);
                }

                if($result['status'] == 200){
                    $successCount++;
                }
            }
        }else{
            $this->showmessage('所选文件类型不正确', site_url('adminpanel/material/index'));
        }

        $this->showmessage('成功上传/更新 '.$successCount, site_url('adminpanel/material/index'));
    }


    private function buildUploadParams($upload_data = '', $type = 'image') {

        if(isset($upload_data['file_ext'])) {
            $params['suffix'] = $upload_data['file_ext'];
        }

        if(isset($upload_data['raw_name'])) {
            $params['path'] = $type.DIRECTORY_SEPARATOR.$upload_data['raw_name'].$upload_data['file_ext'];
        }

        $params ['name'] = isset($upload_data['raw_name'])
                            ? $upload_data['raw_name']
                            : time().random_string(1, 1000000);

        return $params;
    }

    public function edit($id = 0)
    {
        $id = intval($id);
        $material = $this->Material_model->get_one('id='.$id);
        $type_id = $material['material_type_id'];

        $material_type_list = $this->Material_type_model->getMaterialTypeList();

        $select_categorys = $this->Material_type_model->formateMaterialTypeTree($material_type_list, $type_id);


        $this->view('edit', array('require_js' => true, 'is_edit' => true, 'material' => $material, 'select_categorys' => $select_categorys));
    }

    public function update()
    {
        $id          = intval($this->input->post('material_id'));
        $material_name = trim($this->input->post('material_name'));
        $provider = trim($this->input->post('provider'));
        if (!in_array($provider, ['DAHUA', 'HAIKANG', 'YUSHI'])) {

            return $this->showmessage('摄像头类型不正确', site_url('adminpanel/material/index'));
        }

        $custom_id   = trim($this->input->post('custom_id'));
        $material_id  = intval($this->input->post('material_id'));

        $params = ['name' => $material_name, 'custom_id' => $custom_id, 'material_id' => $material_id, 'provider' => $provider];

        $result = $this->Material_model->updatematerial($id, $params);
        
        if (!$result) {
            
            echo json_encode(array('status' => false,'tips'=>"更新相机信息失败")); exit;
        }

        echo json_encode(array('status' => true, 'tips' => "ok")); exit;
    }

    

    public function delete()
    {
        $ids = $this->input->post('pid');
        $result = $this->Material_model->delete_material($ids);
        if (!$result) {

            return $this->showmessage('删除失败', site_url('adminpanel/material/index'));
        }

        return $this->showmessage('删除成功', site_url('adminpanel/material/index'));

    }

}
