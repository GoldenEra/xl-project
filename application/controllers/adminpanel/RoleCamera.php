<?php
/*
*    这是一个演示模块
*/
class RoleCamera extends Admin_Controller {
    
    public function __construct()
    {
        parent::__construct();
        //请在这里初始化模块相关设置

        $this->load->model(array('Role_camera_model'));
        $this->load->model(array('Camera_model'));
        $this->load->model(array('Member_role_model'));
        $this->load->model(array('Address_model'));
    }

    public function setting($role_id = 0, $address_id = 0, $page_no = 1)
    {
        $address_id = $this->security->xss_clean($address_id);
        $address_id = intval($address_id);

        $role_id = $this->security->xss_clean($role_id);
        $role_id = intval($role_id);

        $page_no = $this->security->xss_clean($page_no);
        $page_no = max(intval($page_no),1);
        $count = 50;

        $data_info =$this->Member_role_model->get_one(array('role_id'=>$role_id));
        
        $where = '';
        $orderby = 'created';

        if (is_int($address_id) && !empty($address_id)) {
            $childIDs = $this->Address_model->getChildIDs($address_id);
            $allIDs = array_merge($childIDs, array($address_id));
            $where .= 'address_id in ('. implode(',', $allIDs).')';
        }
        
        $camera_list = $this->Camera_model->listinfo($where, '*',$orderby , $page_no, $count, '',$count, page_list_url('adminpanel/roleCamera/setting/'.$role_id.'/'.$address_id, true));

        if(!$data_info) return $this->showmessage('权限信息不存在');

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $camera_ids = array_column($camera_list, 'id');
            
            $a = $this->Role_camera_model->delete_by_role($role_id, $camera_ids);
            if (!isset($_POST['pid'])) {
                
                $this->showmessage('操作成功', site_url('adminpanel/roleCamera/setting/'.$role_id.'/'.$address_id.'/'.$page_no));
            }
            $insert_camera_ids = $_POST['pid'];
            $result = $this->Role_camera_model->insert_role_camera($role_id, $insert_camera_ids);
            
            $this->showmessage('操作成功', site_url('adminpanel/roleCamera/setting/'.$role_id.'/'.$address_id.'/'.$page_no));

        }elseif(!empty($address_id)){

            $address_info = $this->Address_model->get_one(array('id' => $address_id));
            $provider_arr = ['DAHUA' => '大华', 'YUSHI' => '宇视', 'HAIKANG' => '海康'];
            foreach ($camera_list as &$camera) {
                $camera['role_id'] = 0;
                $camera['camera_id'] = $camera['id'];
                $camera_role_info = $this->Role_camera_model->get_one(['camera_id' => $camera['id'], 'role_id' => $role_id]);
                if ($camera_role_info) {
                    $camera = array_merge($camera, $camera_role_info);
                    $camera['address_detail'] = $this->Address_model->getAddressDetailByID($camera['address_id']);
                }

                $camera['provider_cn'] = isset($provider_arr[$camera['provider']])
                                     ? $provider_arr[$camera['provider']]
                                     : '未知类型';
            }

            $allCameraCount = $this->Address_model->getCameraCount($address_id);
            
            // $this->load->view('adminpanel/setting',$page_data);
            $this->load->view('adminpanel/header');

			$this->load->view('adminpanel/roleCamera/setting', array('require_js'=>true, 'camera_list' => $camera_list, 'data_info' => $data_info, 'pages' => $this->Camera_model->pages, 'address_info' => $address_info, 'allCameraCount' => $allCameraCount,
			       'role_id' => $role_id,
			));

        }else{

            $tree = '';
            $address_list = $this->Address_model->getAddressList();
            $html = $this->Address_model->toTreeWithCameraNum($address_list, 0,  $role_id);
            $this->view('addresstree',array('require_js'=>true, 'html' => $html, 'role_id' => $role_id));
        }
    }

}
