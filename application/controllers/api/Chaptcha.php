<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'core/MY_API_Controller.php';

class Chaptcha extends MY_API_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model(array('Times_model'));
		$this->load->model(array('Captcha_model'));
	}

	public function index_get() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');
		
		switch ($method) {

		case 'generate_captcha':

				$result = $this->Captcha_model->generate_captcha();
				break;
		default:
				break;
		}

		echo json_encode($result);exit;
	}
}