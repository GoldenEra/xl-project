<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'core/MY_API_Controller.php';

class User extends MY_API_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model(array('Times_model'));
		$this->load->model(['User_model']);
		$this->load->model(['Message_model']);
	}

	public function index_get() {

		$method = $this->get('m', '');

		switch ($method) {
			case '':
				
				break;
			
			default:
				# code...
				break;
		}

		echo json_encode($result);exit;
	}

	public function index_post() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');

		switch ($method) {
			case 'login':

				$username = $this->post('username');
				$password = $this->post('password');
				if (!isset($password) || empty($password)) {

					$result = $this->User_model->userMobileLogin($username);
				} else {

					$result = $this->User_model->userLogin($username, $password);
				}
				break;

			case 'add':

				$username = $this->post('username');
				$password = $this->post('password');
				$mobile = $this->post('mobile');
				$id_card = $this->post('id_card');
				$address_id = $this->post('address_id');
				$address_id = !empty($address_id) 
								? $address_id 
								: 1;
								
				$address_detail = $this->post('address_detail');
				$address_detail = !empty($address_detail) 
								? $address_detail
								: '默认地址';

				$result = $this->User_model->addUser($username, $password, $id_card, 
												$mobile, $address_id, $address_detail);
				break;

			// 报警，向同级
			case 'callPoliceDirect':

				$repoter_user_id = $this->post('user_id');
				$address_detail = $this->post('address_detail');
				$type = $this->post('type');
				$content = $this->post('content');
				$latitude = $this->post('latitude');// 经度
				$longitude = $this->post('longitude');// 纬度
				// 110,119,120,other
				set_time_limit(600);
				$image = $video = $thumb = '';
				// 文件上传jpg|jpeg|png
				$allowed_type = ['jpg', 'png', 'jpeg', 'tif', 'mp4', 'rmvb'];	
				if (!empty($_FILES)) {
					foreach ($_FILES as $key => $value) {
						if (is_uploaded_file($value['tmp_name'])) {

							$uploads_dir = UPLOAD_PATH.$key.DIRECTORY_SEPARATOR;

	                        $tmp_name = $value['tmp_name'];

							$file_name = explode(".", $value["name"]);
							// 文件后缀
							$file_ext = end($file_name);
							if(!in_array($file_ext, $allowed_type)) {
							
								continue;
							}
							$randomSting = $this->Message_model->generateRandomString(5);
							$newfilename = round(microtime(true)) .$randomSting.'.' . end($file_name);
	                        $destination = $uploads_dir.$newfilename;

	                        move_uploaded_file($tmp_name, $destination);
							error_log(date('Y-m-d H:i:s', time())." 文件 $newfilename 上传到 $destination 成功\n\n", 3, LOG_PATH.'uplod.log');
	                        
							$relative_path = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.$newfilename;
	                        $image = $key == 'image' ? $relative_path : $image;
	                        $video = $key == 'video' ? $relative_path : $video;
	                        $thumb = $key == 'thumb' ? $relative_path : $thumb;
	                    }
					}
				}
				
				$result = $this->User_model->callPolice($repoter_user_id, $type, $content, $latitude, $longitude, $address_detail, $image, $video, $thumb);
				break;

			// 信息上报，当前级别不能处理该信息时，信息上报给上一级
			case 'toLeader':
				$message_id = $this->post('message_id');
				$user_id = $this->post('user_id');
				
				$priv = $this->User_model->checkPriv($user_id, 'toLeader');

				if ($priv['status'] == 200) {
					# code...
					$result = $this->Message_model->toLeader($message_id, $user_id);

				} else{
					$result = $priv;
				}

				break;

			// 处理消息
			case 'processMessage':
			case 'finishMessage':

				$user_id = $this->post('user_id');
				$message_id = $this->post('message_id');
				$remark = $this->post('remark');

				$priv = $this->User_model->checkPriv($user_id, 'updateMessageStatus');

				if ($priv['status'] == 200) {
					$status = ($method == 'processMessage') ? 'PROCESS' : 'FINISH';

        			$result = $this->Message_model->updateStatus($message_id, $status, $user_id);
					
				} else{
					$result = $priv;
				}
				
				break;

			default:
				break;
		}

		echo json_encode($result); exit;
	}

}
