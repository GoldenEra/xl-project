<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'core/MY_API_Controller.php';

class Camera extends MY_API_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model(array('Times_model'));
		$this->load->model(['Camera_model']);
		$this->load->model(['Material_model']);
	}

	public function index_get() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');
		
		switch ($method) {

		case 'list':
				$page = $this->query('p');
				$limit = $this->query('limit');
				$limit = !empty($limit)
						 ? $limit
						 : 20;

				$result = $this->Camera_model->add($camera_name, $address_id);
				break;
		case 'importDaHuaCameras':
				// $file_name = $this->query('file_name');
				$result = $this->Camera_model->importDaHuaCameras();
				break;

		// case 'haiKangFirstToken':
		// 		$result = $this->Camera_model->haiKangFirstToken();
		// 		var_dump($result);
		// 		exit;

		// case 'haiKangSecondToken':
		// 		$result = $this->Camera_model->haiKangSecondToken();
		// 		var_dump($result);
		// 		exit;

		// case 'haiKangAllResource':
		// 		$result = $this->Camera_model->haiKangAllResource();
		// 		var_dump($result);
		// 		exit;

		// case 'haiKangResourceToTree':
		// 		$result = $this->Camera_model->haiKangResourceToTree();
		// 		echo $result;
		// 			exit;

		// case 'getAllResourceOrg':
		// 		$result = $this->Camera_model->getAllResourceDetailByOrg();
		// 		echo $result;
		// 		exit;

		case 'importHaiKangCameras':
				$result = $this->Camera_model->importHaiKangCameras();

		// case 'saveCamerasByIndexCode':
		// 		$result = $this->Camera_model->saveCamerasByIndexCode();
		// 		var_dump($result);
		// 		exit;

		// case 'getCameraStreamServiceByNetZone':
		// 		$result = $this->Camera_model->getCameraStreamServiceByNetZone();
		// 		var_dump($result);
		// 		exit;

		default:
				break;
		}

		echo json_encode($result);exit;
	}

	public function index_post() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');

		switch ($method) {

			case 'add':

				$camera_name = $this->post('camera_name');
				$address_id = $this->post('address_id');
				$address_id = !empty($address_id) 
								? $address_id 
								: 1;

				$result = $this->Camera_model->add($camera_name, $address_id);
				break;

			case 'list':
			
				$user_id = $this->post('user_id');
				$page = $this->post('page');

				$result = $this->Camera_model->cameraList($user_id, $page);
				break;

			case 'updateStatus':
			
				$custom_id = $this->post('custom_id');
				$status = $this->post('status');

				$result = $this->Camera_model->updateStatus($custom_id, $status);
				break;

			case 'updateDeviceStatus':
			
				$c_device_code 	= $this->post('c_device_code');
				$c_device_status 	= $this->post('c_device_status');

				$result = $this->Camera_model->updateDeviceStatus($c_device_code, $c_device_status);
				break;


			default:
				break;
		}

		echo json_encode($result); exit;
	}
}