<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'core/MY_API_Controller.php';

class Material extends MY_API_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model(['Times_model']);
		$this->load->model(['Material_model']);
		$this->load->model(['Material_type_model']);
	}

	public function index_post() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');
		$data = [];
		
		switch ($method) {

		// 一级分类一个
		case 'listOneLevel':
				
			$data['list'] = [
						['id' => 1, 'name' => '一级分类1', 'image' => ''],
						['id' => 2, 'name' => '一级分类2', 'image' => '']
			];

			$data = $this->Material_type_model->getLevelOntList();
			if ($data) {
				foreach ($data as &$detail) {
					$detail['image'] = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$detail['icon'];
				}
				$data = [
					'list' => $data,
					'total' => count($data)
				];
				$result = ['data' => $data, 'status' => 200, 'tips' => '成功'];
			} else {

				$result =  ['status' => -1, 'list' => '', 'tips' => '无内容'];
			}

			break;
		// 二级分类一个
		case 'listTwoLevel':

			$levelOneID = $this->post('levelOneID');

			$typeDetail = $this->Material_type_model->getByLevelOneID($levelOneID);
			if ($typeDetail) {
				foreach ($typeDetail as &$detail) {
					$detail['image'] = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$detail['icon'];
				}
				$data = [
					'list' => $typeDetail,
					'total' => count($typeDetail)
				];
				$result = ['data' => $data, 'status' => 200, 'tips' => '成功'];
			} else {

				$result =  ['status' => -1, 'list' => '', 'tips' => '无内容'];
			}

			break;

			
			// $data['list'] =  [
			// 		['id' => 3, 'parent_id' => 1, 'name' => '二级分类1', 'image' => ''],
			// 		['id' => 4, 'parent_id' => 1, 'name' => '二级分类2', 'image' => '']
			// 	];
			// $data['total'] = 2;
			// $result = [
			// 	'status' => 200,
			// 	'data'	 => $data,
			// 	'tips' => '获取成功'
			// ];
		// 二级分类下面的列表一个
		case 'list':

				$levelTwoID = $this->post('levelTwoID');
				$data = $this->Material_model->getByTypeID($levelTwoID);
				
				if ($data) {
				foreach ($data as &$detail) {
					if ($detail['type'] == 'video') {

						$detail['video'] = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$detail['path'];
						if (!empty($detail['thumb'])) {
							# code...
							$detail['thumb'] = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$detail['thumb'];
						}
						
					} else{

						$detail['image'] = DIRECTORY_SEPARATOR.UPLOAD_FOLDER_NAME.DIRECTORY_SEPARATOR.$detail['path'];
					}
				}
				$data = [
					'list' => $data,
					'total' => count($data)
				];
				$result = ['data' => $data, 'status' => 200, 'tips' => '成功'];
				} else {

					$result =  ['status' => -1, 'list' => '', 'tips' => '无内容'];
				}


				// $data['list'] = [
				// 	['id' => 1, 'name' => '二级分类详情1', 'type' => 'image', 'path' => '', 'image' => ''],
				// 	['id' => 2, 'name' => '二级分类详情1', 'type' => 'video', 'path' => '', 'image' => ''],
				// ];
				// $data['total'] = 2;

				break;
		default:
				break;

		}
		
		echo json_encode($result);exit;
	}


	public function index_get() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];

		$method = $this->query('m');
		
		switch ($method) {

		case 'listOneLevel':
				
				$data = [
							['name' => '一级分类1',],
							['name' => '一级分类2']
				];
				$result = ['status' => 200, 'data' => $data, 'tips' => 'no method found'];

				break;
		case 'listTwoLevle':

				$levelOneID = $this->query('levelOneID');

				$result = [
					'status' => 200,
					'data'	 => [
						['name' => '二级分类1'],
						['name' => '二级分类2']
					],
					'tips' => '获取成功'
					
				];

		case 'list':

				$levelOneID = $this->query('levelTwoID');

				$data = [
					['id' => 1, 'name' => '二级分类详情1', 'type' => 'image', 'path' => ''],
					['id' => 2, 'name' => '二级分类详情1', 'type' => 'video', 'path' => ''],
				];

			$result = ['status' => 200, 'data' => $data, 'tips' => 'no method found'];


		default:
				break;


		}
		
		echo json_encode($result);exit;
	}
}
