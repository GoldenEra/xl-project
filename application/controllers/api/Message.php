<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'core/MY_API_Controller.php';

class Message extends MY_API_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model(array('Times_model'));
		$this->load->model(['Message_model']);
		$this->load->model(['SMS_send_model']);
	}
	public function index_post() {

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
		$method = $this->query('m');

		switch ($method) {
			case 'list':

				$user_id = $this->post('user_id');
				$page = $this->post('page');

				$result = $this->Message_model->listMessage($user_id, $page);
				break;

			case 'autoToLeader':

				$result = $this->Message_model->autoToLeader();

				break;

			case 'myMessage':

				$user_id = $this->post('user_id');
				$page = $this->post('page');

				$result = $this->Message_model->myMessage($user_id, $page);
				break;

			case 'sendSMS':
					// 时间戳
				$number = $this->post('number');
				// $a = $this->SMS_send_model->sendSMS($number);
				// var_dump($a);exit;
				break;

			default:
				break;
		}

		echo json_encode($result); exit;
	}


	public function index_get(){

		$result = ['status' => -1, 'data' => '', 'tips' => 'no method found'];
			$method = $this->query('m');

			switch ($method) {
				case 'list':

					$user_id = $this->post('user_id');
					$page = $this->post('page');

					$result = $this->Message_model->listMessage($user_id, $page);
					break;

				case 'autoToLeader':

					$result = $this->Message_model->autoToLeader();

					break;

				default:
					break;
			}

		echo json_encode($result); exit;
	}
}
