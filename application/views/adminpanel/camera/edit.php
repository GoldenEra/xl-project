<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?>

<form class="form-horizontal" role="form" id="validateform" name="validateform" method="post" action="<?php echo current_url()?>" >
<div class='panel panel-default'>
	<div class='panel-heading'>
		<i class='icon-edit icon-large'></i>
		<?php echo $is_edit?"修改":"新增"?>摄像头信息
		<div class='panel-tools'>

			<div class='btn-group'>
				<?php aci_ui_a($folder_name,'user','index','',' class="btn  btn-sm "','<span class="glyphicon glyphicon-arrow-left"></span> 返回')?>
			</div>
		</div>
	</div>
	<div class='panel-body'>
		<fieldset>
				<legend>摄像头的基本信息</legend>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">摄像头名字</label>
						<div class="col-sm-4">
							<input type="text" name="camera_name" value="<?php echo $camera['camera_name']?>" id="camera_name"  class="form-control validate[required]"  placeholder="请输入名字" >
						</div>
					</div>

					<div class="form-group">
		                <label class="col-sm-2 control-label">摄像头来源</label>
		                <div class="col-sm-4">
		                    <select name="provider" id="provider" class=" form-control">
		                        <option value="HAIKANG" <?php echo $camera['provider'] == 'HAIKANG' ? 'selected' : ''; ?>>海康</option>
		                        <option value="DAHUA" <?php echo $camera['provider'] == 'DAHUA' ? 'selected' : ''; ?>>大华</option>
		                        <option value="YUSHI" <?php echo $camera['provider'] == 'YUSHI' ? 'selected' : ''; ?>>宇视</option>
		                    </select>
		                </div>
		            </div>

					<div class="form-group">
						<label class="col-sm-2 control-label">摄像头ID</label>
						<div class="col-sm-4">
							<input type="text" name="custom_id" value="<?php echo $camera['custom_id']?>" id="custom_id"  class="form-control validate[required]"  placeholder="请输入ID" >
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="col-sm-2 control-label">摄像头类型</label>
						<div class="col-sm-4">
							<input type="text" name="camera_type" value="<?php echo $camera['camera_type']?>" id="camera_type"  class="form-control validate[required]"  placeholder="请输入摄像头类型" >
						</div>
					</div> -->

					 <div class="form-group">
		                <label class="col-sm-2 control-label">请绑定区域区域</label>
		                <div class="col-sm-4">
		                    <select name="address_id" id="address_id" class=" form-control">
		                        <option value="0">无</option>
		                        <?php echo $select_categorys; ?>
		                    </select>
		                </div>
		            </div>

		            <!-- 隐藏域里加入ID -->
            	<?php if (isset($camera['id'])): ?>

                <input type="hidden" name="camera_id" value="<?php echo isset($camera['id']) ? $camera['id'] : ''  ?>"  class="form-control"/>
                
            	<?php endif ?>


			</fieldset>

		<div class='form-actions'>
			<?php aci_ui_button($folder_name,'user','edit',' type="submit" id="dosubmit" class="btn btn-primary " ','保存')?>
		</div>
     </div>

</form>
<script language="javascript" type="text/javascript">

	var folder_name = "<?php echo $folder_name?>";
	var id = <?php echo isset($camera['id']) ? $camera['id'] : 0; ?>

	require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
		require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/edit.js']);
	});
</script>
