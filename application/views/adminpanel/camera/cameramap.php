<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?>

<script type="text/javascript">
    var camera = <?php echo json_encode($camera_list);?>;
    var toLeaderPriv = <?php echo $toLeaderPriv;?>;
	  console.log(camera);
    var updateMessageStatusPriv = <?php echo $updateMessageStatusPriv;?>;
	// console.log(updateMessageStatusPriv);
</script>
<div class='filter_button'>
  
   <?php 
    
      echo aci_ui_a($folder_name,$controller_name,'messageMap', 'camera',' class="btn btn-primary"','查看摄像头',true);
      echo aci_ui_a($folder_name,$controller_name,'messageMap', 'message',' class="btn btn-primary"','查看报警信息',true);
      echo aci_ui_a($folder_name,$controller_name,'messageMap', 'user',' class="btn btn-primary"','查看用户信息',true);
      echo aci_ui_a($folder_name,$controller_name,'messageMap', 'all',' class="btn btn-primary"','查看全部',true);
    ?>
</div>
<style type="text/css">
  .filter_button{
    text-align: center;
    margin-bottom: 20px;
  }
  .filter_button a{
    margin-left: 10px;
  }
</style>
<div id="container" style="width:98%; height:1000px"></div>
   <!--<script src="http://webapi.amap.com/js/marker.js"></script>-->
    <script src="http://webapi.amap.com/maps?v=1.3&key=47828182955d0b90791b58ba17c4fa0f"></script>
   <script type="text/javascript">
   	
   	function markerClick(e){
	    infoWindow.setContent(e.target.content);
	    infoWindow.open(map, e.target.getPosition());
	}
   </script>

   <script type="text/javascript">

        var map = new AMap.Map('container',{resizeEnable: true,zoom:4});
        var markers = []; //province见Demo引用的JS文件
        var infoWindow = new AMap.InfoWindow();
        
        var redicon = new AMap.Icon({
            image : 'http://webapi.amap.com/theme/v1.3/markers/b/mark_rs.png',//24px*24px
            size : new AMap.Size(19,33),
            imageSize : new AMap.Size(19,33)
        });

        // 如果是摄像头，则图标使用摄像头的图片
        var cameraicon = new AMap.Icon({  

            image: "../../../images/camera.png",
            size : new AMap.Size(30,33),
            imageSize : new AMap.Size(30,33)
        });

        // 如果是用户，则用用户的图片
        var usericon = new AMap.Icon({  

            image: "../../../images/user.png",
            size : new AMap.Size(30,33),
            imageSize : new AMap.Size(20,33)
        });

        for (var i = 0; i < camera.length; i += 1) {
    		  var marker;
      		if (camera[i].show_type == 'message') {

              marker = new AMap.Marker({
                  position: camera[i].center,
                  title: camera[i].name,
                  zindex: 101,
                  map: map,
              });
    				  // 有图片和报警信息,将报警信息写入弹出框
              var info = []; 
              info.push('<div>报警时间：'+camera[i].created+'</div>');
              info.push('<div>报警人员：'+camera[i].user_info.username+'</div>');
              info.push('<div>内容：'+(camera[i].name || '')+'</div>'+'<img src="'+(camera[i].image || '')+'">');
              info.push('<div>报警地点：'+camera[i].address_detail+'</div>');
              info.push('<div>类别：'+camera[i].type_cn+'</div>');
              info.push('<div>联系方式：'+camera[i].user_info.mobile+'</div>');
              button = '';
              if(camera[i].status == 'CREATED'){
                  // 信息才创建，使用红色图钉，正在处理用蓝色图钉（默认图钉）
                  marker = new AMap.Marker({
                    position: camera[i].center,
                    title: camera[i].name,
                    map: map,
                    zindex: 101,
                    icon: redicon
                  });

                  if (toLeaderPriv) {
                    button += '<div class="up-link"><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/toLeader/'+camera[i].id+'">信息上报</a>';
                  }
                  if (updateMessageStatusPriv) {
                    button += '<a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/updateMessageStatus/'+camera[i].id+'/PROCESS">开始处理</a></div>';
                  }
                  // info.push('<div class="up-link"><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/toLeader/'+camera[i].id+'">信息上报</a><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/updateMessageStatus/'+camera[i].id+'/PROCESS">开始处理</a></div>');
              }

              if(camera[i].status == 'PROCESS' && camera[i].receive_user_id == <?php echo $this->user_id;?>){

                  if (toLeaderPriv) {
                    button += '<div class="up-link"><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/toLeader/'+camera[i].id+'">信息上报</a>';
                  }
                  if (updateMessageStatusPriv) {
                    button += '<a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/updateMessageStatus/'+camera[i].id+'/FINISH">开始处理</a></div>';
                  }
                // info.push('<div class="up-link"><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/toLeader/'+camera[i].id+'">信息上报</a><a class="btn btn-default btn-s delete_confirm" href="/adminpanel/message/updateMessageStatus/'+camera[i].id+'/FINISH">完成处理</a></div>');
              }

              // console.log(marker.content);
         			// marker.content = '<div>'+camera[i].name+'</div>';
			  		marker.content = info.join('<br>');
			  		marker.content += button;
      			marker.on('click', markerClick);
      		} 

          if(camera[i].show_type == 'camera') {

      				marker = new AMap.Marker({
      					position: camera[i].center,
      					title: camera[i].name,
      					map: map,
                icon: cameraicon
      				});

    				  // 点击标签的信息窗体显示
    				  marker.content = camera[i].name;
    				  marker.on('click', markerClick);
    			}

          if(camera[i].show_type == 'user') {
              console.log(camera[i]);
              marker = new AMap.Marker({
                position: camera[i].center,
                title: camera[i].name,
                map: map,
                icon: usericon
              });

              // 点击标签的信息窗体显示
              marker.content = camera[i].name;
              marker.on('click', markerClick);
          }

    			markers.push(marker);
        }
        
			  map.setFitView();
    </script>
    <script type="text/javascript" src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>
    <script type="text/javascript">
      require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
      require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/message/index.js']);
    });
    </script>
    <style type="text/css">
      .amap-info-content {
        /* text-align: center; */
        width: 100%;
        height: 100%;
      }
      .amap-info-content img{
        width: 100%;
      }
      .amap-info-content .up-link{
        text-align: center;
      }
      .amap-info-content a{
        display: inline-block;
      }



    </style>
