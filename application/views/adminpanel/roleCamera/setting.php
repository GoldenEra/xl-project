<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?>
    <form method="post" id="form_list" action="<?php echo current_url() ?>">
        <div class=''>

            <fieldset>
                <legend data-address_id=<?php echo $address_info['id'] ?>><?php echo $address_info['name']; ?>&nbsp;列表</legend>
                <table class="table table-hover dataTable">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th nowrap="nowrap">用户组</th>
                        <th nowrap="nowrap">摄像头名</th>
                        <th nowrap="nowrap">摄像头所属</th>
                        <th>权限</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 0; foreach ($camera_list as $key => $value): ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo $data_info['role_name']; ?></td>
                            <td><?php echo $value['camera_name']; ?></td>
                            <td><?php echo $value['provider_cn']; ?></td>
                            <td>
                                <input type='checkbox' name='pid[]' value="<?php echo $value['camera_id']; ?>" <?php echo !empty($value['role_id']) &&
                                    $value['role_id'] == $role_id
                                    ? 'checked' : ""; ?>  />
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </fieldset>

            <div class="panel-footer">
              <div class="pull-left">
                    
                <div class="btn-group">
                    <?php aci_ui_button($folder_name, 'roleCamera', 'setting', 'type="submit" id="dosubmit" class="btn btn-primary "', '保存设置') ?>
                  <button type="button" class="btn btn-default" id="reverseBtn" ><span class="glyphicon glyphicon-ok"></span> 反选</button>
                 
                </div>
              </div>
              <div class="pull-right">
                  <?php echo $pages; ?>
              </div>
            </div>
        </div>
    </form>  
</div>

<script type="text/javascript">
    var SITE_URL = "<?php echo BASE_URL(); ?>";
</script>
<script type="text/javascript" src='/scripts/require.js'></script>
<script language="javascript" type="text/javascript">
  var group_id = "<?php echo $this->group_id;?>"
  var folder_name="<?php echo $folder_name?>";
  var controller_name ="<?php echo $controller_name?>";
  require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
    require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/setting.js']);
  });

</script>
