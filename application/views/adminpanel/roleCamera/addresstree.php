<div class='panel panel-default'>
  <div class='panel-heading'>
    <i class='glyphicon glyphicon-list'></i>&nbsp;摄像头权限管理--地址选择
    <div class='panel-tools'>
      <div class='btn-group'>
        <?php aci_ui_a($folder_name, 'role','index','',' class="btn btn-sm pull-right"','<span class="glyphicon"></span> 返回上一页')?>
        <a class='btn' href='<?php echo current_url() ?>'>
          <i class='glyphicon glyphicon-refresh'></i>
          刷新
        </a>
      </div>
    </div>
  </div>
</div>

<div id="container" class="col-sm-3 col-md-2">
  <?php echo $html; ?>
</div>

<iframe id='myiframe' src="/adminpanel/roleCamera/setting/<?php echo $role_id;?>/1">
</iframe>

<style type="text/css">
  #container{
    position: fixed;
    left: -2px;
    top: 117px;
    z-index: 1111;
    max-height: 80%; 
    overflow: auto;
  }
  #myiframe {
    width: 90%;
    height: 1000px;
    margin-left: 20px;
    border: gray;
	overflow: auto;
  }
</style>

<script language="javascript" type="text/javascript">
  var group_id = "<?php echo $role_id;?>"
  var folder_name="<?php echo $folder_name?>";
  var controller_name ="<?php echo $controller_name?>";
  require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
    require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/setting.js']);
  });
</script>
