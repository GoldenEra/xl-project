<div class='panel panel-default'>
    <div class='panel-heading'>
        <i class='glyphicon glyphicon-edit'></i>分类管理—<?php echo $is_add?"新增分类":"修改分类"?>
        <div class='panel-tools'>
            <div class='btn-group'>
                <a class='btn' href='<?php echo current_url() ?>'>
                    <i class='glyphicon glyphicon-refresh'></i>
                    刷新
                </a>
                <?php aci_ui_a($folder_name, $controller_name, 'index', '', ' class="btn btn-sm pull-right"', '<span class="glyphicon glyphicon-arrow-left"></span> 返回') ?>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <form name="validateform" id="validateform" class="form-horizontal"
                enctype="multipart/form-data"
              action="<?php echo site_url($folder_name.'/'.$controller_name.'/add') ?>" method="post">
            
            <div class="form-group">
                <label class="col-sm-2 control-label">分类名称</label>

                <div class="col-sm-5">
                    <input type="text" name="material_type_name" value="<?php echo isset($current_material['name']) ? $current_material['name'] : ''  ?>"  class="validate[required] form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">请<?php echo $is_add ? '添加' : '更新';?>图标</label>
                <div class="col-sm-10">
                  <input type="file" id="material_type_icon" name="material_type_icon">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">请选择父分类</label>
                <div class="col-sm-5">
                    <select name="type_id" id="type_id" class=" form-control">
                        <option value="0">根分类</option>
                        <?php echo $select_categorys; ?>
                    </select>
                </div>
            </div>

            <!-- 隐藏域里加入ID -->
            <?php if (isset($current_material['id'])): ?>

                <input type="hidden" name="material_type_id" value="<?php echo isset($current_material['id']) ? $current_material['id'] : ''  ?>"  class="form-control"/>
                
            <?php endif ?>
            
            <div class='form-actions'>

                <?php aci_ui_button($folder_name,$controller_name, 'add', ' type="submit" id="dosubmit" class="btn btn-primary" ', '保存') ?>
            </div>
        </form>
    </div>
</div>

<script language="javascript" type="text/javascript">

    var add =<?php echo $is_add ? "true" : "false"?>;
    var folder_name="<?php echo $folder_name?>";
    var controller_name ="<?php echo $controller_name?>";
    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
        require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/add.js']);
    });
</script>