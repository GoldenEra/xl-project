<?php defined('IN_ADMIN') or exit('No permission resources.'); ?>

<!-- <div class="panel" style="background: #F5F5F5;">
    <div class="panel-body">
        <div class="row placeholders">
            <img style="width: 44%;" src="../../../../images/index.png">
        </div>
    </div>
</div> -->
 
 <div class="panel">
     <div class="panel-body">
         <div class="row placeholders">

             <?php if(!empty($show)): foreach ($show as $k => $v): ?>
                 <div class="col-xs-12 col-sm-3 top">
                     <a href="<?php echo $v['url'] ?>"><p class="circle"><i class="fa fa-<?php echo $v['icon'] ?> fa-4x"></i></p>
                     <h4><?php echo $v['name'] ?></h4></a>
                 </div>
                <?php endforeach; ?>
             <?php endif; ?>
         </div>
     </div>
 </div>
 <style type="text/css">
     .top{
        margin-top: 50px;
     }
 </style>
 <script language="javascript" type="text/javascript">
    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
        require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/index.js']);
    });
</script>
