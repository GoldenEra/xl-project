<?php defined('IN_ADMIN') or exit('No permission resources.'); ?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- <a href="#" ><div class="tsLogo"></div></a> -->
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <form class="form-signin" role="form" action="<?php echo current_url()?>"  method="post" id="validateform" name="validateform">
        <div class="form-signin-body">
            <div class="login-text">用户登录</div>
            <div class="form-group">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" id="username" name="username" class="form-control" placeholder="请输入管理手机号" autofocus>
            </div>

            <div class="form-group">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" id="password" name="password" class="form-control" placeholder="请输入管理密码"  autofocus>
            </div>

            <div class="form-group">
                <input type="text" id="cap" name="cap" placeholder="输入验证码">
                <span class="reload-captcha">
                    <img class="captcha-img" src="<?php echo $filename?>" style="width: 150; height: 40; border: 0;" alt=" ">
                </span>
                
            </div>

            <button class="btn  btn-primary btn-block" type="submit" id="dosubmit" disabled="disabled">登录</button>

        </div>
    </form>
    <style type="text/css">
        .login-text{
            text-align: center;
            font-size: 20px;
            margin-bottom: 25px;
            margin-right: 10px;
        }
        #cap{
            width: 180px;
            height: 33px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
            require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/login.js']);
        });
    </script>