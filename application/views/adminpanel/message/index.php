<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?>
<div class='panel panel-default grid'>
    <div class='panel-heading'>
        <i class='glyphicon glyphicon-th-list'></i> 成员列表
        <div class='panel-tools'>

            <div class='btn-group'>
                <?php aci_ui_a($folder_name, 'user', 'add', '', ' class="btn  btn-sm "', '<span class="glyphicon glyphicon-plus"></span> 添加') ?>
            </div>
            <div class='badge'><?php echo count($data) ?></div>
        </div>
    </div>
    <div class='panel-filter '>
        <form class="form-inline" role="form" method="get">
            <div class="form-group">
                <label for="keyword" class="form-control-static control-label">关键词</label>
                <input class="form-control" type="text" name="keyword" value="<?php echo $keyword; ?>" id="keyword"
                       placeholder="请输入关键词"/></div>
            <button type="submit" name="dosubmit" value="搜索" class="btn btn-success"><i
                    class="glyphicon glyphicon-search"></i></button>
        </form>
    </div>
    <form method="post" id="form_list">

            <div class='panel-body '>


                <table class="table table-hover dataTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>报告者名字</th>
                        <th>报告详细地址</th>
                        <th>报报警类别</th>
                        <th>报告时间</th>
                        <th>信息状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>

        	<?php if (!empty($data['list'])): ?>
                    <tbody>
                    <?php foreach ($data['list'] as $k => $v): ?>
                        <tr>
                            <td><?php echo ++$k; ?></td>
                            <td><?php echo isset($v['user_info']['username']) ? $v['user_info']['username'] : '未知用户'; ?></td>
                            <td><?php echo $v['address_detail']; ?></td>
                            <td><?php echo $v['type_cn']; ?></td>
                            <td><?php echo $v['created']; ?></td>
                            <td><?php echo $v['status_cn']; ?></td>
                            <td>
                                <?php aci_ui_a($folder_name, 'message', 'detail', $v['id'], ' class="btn btn-default btn-xs"', '<span class="glyphicon glyphicon-edit"></span> 查看详情') ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
		<?endif;?>
                </table>
            </div>

            <div class="panel-footer">
                <div class="pull-right">
                    <?php echo $data['page_url']; ?>
                </div>
            </div>

         </form>
</div>
</div>

<script language="javascript" type="text/javascript">
    var folder_name = "<?php echo $folder_name?>";
    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
        require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/index.js']);
    });
</script>
