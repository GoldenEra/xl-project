<div class='panel panel-default'>
    <div class='panel-heading'>
        <i class='glyphicon glyphicon-edit'></i>报警详情
        <div class='panel-tools'>
            <div class='btn-group'>
                <a class='btn' href='<?php echo current_url() ?>'>
                    <i class='glyphicon glyphicon-refresh'></i>
                    刷新
                </a>
                <?php aci_ui_a($folder_name, $controller_name, 'index', '', ' class="btn btn-sm pull-right"', '<span class="glyphicon glyphicon-arrow-left"></span> 返回') ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form name="validateform" id="validateform" class="form-horizontal"
                enctype="multipart/form-data"
              action="<?php echo site_url($folder_name.'/'.$controller_name.'/update') ?>" method="post">
            <style type="text/css">
                p {
                    margin-top: 7px;
                }
            </style>
            <div class="form-group">
                <label class="col-sm-2 control-label">报告者名字</label>
                <div class="col-sm-5">
                    <p><?php echo isset($data['user_info']['username']) ? $data['user_info']['username'] : '未知用户'; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">详细地址</label>
                <div class="col-sm-5">
                    <p><?php echo $data['address_detail']; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">报告内容</label>
                <div class="col-sm-5">
                    <p><?php echo $data['content']; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">报告时间</label>
                <div class="col-sm-5">
                    <p class="vertical-align: middle;"><?php echo $data['created']; ?></p>
                </div>
            </div>

           
            <div class="form-group">
                <label class="col-sm-2 control-label">状态</label>
                <div class="col-sm-5">
                    <p class="vertical-align: middle;"><?php echo $data['status_cn']; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">类别</label>
                <div class="col-sm-5">
                    <p class="vertical-align: middle;"><?php echo $data['type_cn']; ?></p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">图片</label>
                <div class="col-sm-8">
                    <?php if(!empty($data['image'])){ ?>
                        <img style="width: 100%;" src="<?php echo $data['image'];?>">
                    <?php }else{ ?>
                        <p>没有图片</p>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">视频</label>
                <div class="col-sm-5">
                    <?php if(!empty($data['video'])){ ?>
                        <video width="320" height="240" controls>
                          <source src="<?php echo $data['video'];?>" type="video/mp4">
                          <source src="<?php echo $data['video'];?>" type="video/ogg">
                          浏览器暂不支持该视频
                        </video>
                    <?php }else{ ?>
                        <p>没有视频</p>
                    <?php } ?>
                </div>
            </div>

            <!-- 隐藏域里加入ID -->
            <?php if (isset($current_material['id'])): ?>

                <input type="hidden" name="material_type_id" value="<?php echo isset($current_material['id']) ? $current_material['id'] : ''  ?>"  class="form-control"/>
                
            <?php endif ?>
            
            <!-- <div class='form-actions'>

                <?php aci_ui_button($folder_name,$controller_name, 'add', ' type="submit" id="dosubmit" class="btn btn-primary" ', '保存') ?>
            </div> -->
        </form>
        <!-- 信息出于未处理状态，显示处理信息和信息上报两个按钮 -->
        <?php if ($data['status'] == 'CREATED') {
            aci_ui_a($folder_name, 'message', 'updateMessageStatus', $data['id'].'/PROCESS', ' class="btn btn-default btn-l delete_confirm"', '<span class="glyphicon glyphicon-edit"></span> 开始处理');
            aci_ui_a($folder_name, 'message', 'toLeader', $data['id'], ' class="btn btn-default btn-l delete_confirm"', '<span class="glyphicon glyphicon-edit"></span> 信息上报');

        } ?>
        
        <!-- 对于正在处理状态信息，只有之前处理过的人才有资格操作，其他人只有查看权限 -->
        <?php if ($data['status'] == 'PROCESS' && $data['receive_user_id'] == $this->user_id) {
            aci_ui_a($folder_name, 'message', 'updateMessageStatus', $data['id'].'/FINISH', ' class="btn btn-default btn-l delete_confirm"', '<span class="glyphicon glyphicon-edit"></span> 完成处理');
            aci_ui_a($folder_name, 'message', 'toLeader', $data['id'], ' class="btn btn-default btn-l delete_confirm"', '<span class="glyphicon glyphicon-edit"></span> 信息上报');
        } ?>

    </div>
</div>


<script language="javascript" type="text/javascript">

    var folder_name="<?php echo $folder_name?>";
    var controller_name ="<?php echo $controller_name?>";
    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
    require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/index.js']);
    });

   
</script>