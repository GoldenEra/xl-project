<div class='panel panel-default'>
    <div class='panel-heading'>
        <i class='glyphicon glyphicon-edit'></i> 地址管理—<?php echo $is_add?"新增地址":"修改地址"?>
        <div class='panel-tools'>
            <div class='btn-group'>
                <a class='btn' href='<?php echo current_url() ?>'>
                    <i class='glyphicon glyphicon-refresh'></i>
                    刷新
                </a>
                <?php aci_ui_a($folder_name, $controller_name, 'indexTree', '', ' class="btn btn-sm pull-right"', '<span class="glyphicon glyphicon-arrow-left"></span> 返回') ?>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <form name="validateform" id="validateform" class="form-horizontal"
              action="<?php echo site_url($folder_name.'/'.$controller_name.'/add') ?>" method="post">


            <!-- <div class="form-group">
                <label class="col-sm-2 control-label">上级地址</label>
                <div class="col-sm-5">
                    <select name="parent_id" id="parent_id" class=" form-control">
                        <option value="0">无</option>
                        <?php echo $select_categorys; ?>
                    </select>
                </div>
            </div> -->
            

            <div class="form-group">
                <label class="col-sm-2 control-label">上级地址</label>
                <div class="col-sm-5">
                    <div id="container">
                      <?php echo $treeHtml; ?>
                    </div>
                    <input type="hidden" name="parent_id_tree" id='tree_parent_id' value=""  class="form-control"/>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-2 control-label">地址名称</label>

                <div class="col-sm-5">
                    <input type="text" name="address_name" value="<?php echo isset($current_address['name']) ? $current_address['name'] : ''  ?>"  class="validate[required] form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">邮政编码</label>

                <div class="col-sm-5">
                    <input type="text" name="zipcode" value="<?php echo isset($current_address['zipcode']) ? $current_address['zipcode'] : ''  ?>"  class="form-control"/>
                </div>
            </div>
            <!-- 隐藏域里加入ID -->
            <?php if (isset($current_address['id'])): ?>

                <input type="hidden" name="address_id" value="<?php echo isset($current_address['id']) ? $current_address['id'] : ''  ?>"  class="form-control"/>
                
            <?php endif ?>
            
            <div class='form-actions'>
                <?php 
                if (!$is_add) {
                    
                    echo aci_ui_a($folder_name,$controller_name,'delete', $current_address['id'],' class="btn btn-primary"','删除',true);
                } else {
                    echo aci_ui_button($folder_name,$controller_name, 'add', ' type="submit" id="dosubmit" class="btn btn-primary" ', '保存');
                }
                
                 ?>
            </div>
        </form>
    </div>
</div>


<script language="javascript" type="text/javascript">

    var add =<?php echo $is_add?"true":"false"?>;
    var folder_name="<?php echo $folder_name?>";
    var controller_name ="<?php echo $controller_name?>";
    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
        require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/add.js']);
    });
</script>