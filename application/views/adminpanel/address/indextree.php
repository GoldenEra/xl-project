<div class='panel panel-default'>
  <div class='panel-heading'>
    <i class='glyphicon glyphicon-list'></i> 地址管理
    <div class='panel-tools'>
      <div class='btn-group'>
        <a class='btn' href='<?php echo current_url() ?>'>
          <i class='glyphicon glyphicon-refresh'></i>
          刷新
        </a>
        <?php //aci_ui_a($folder_name,$controller_name,'addPage','0',' class="btn btn-sm pull-right"','<span class="glyphicon glyphicon-plus"></span> 添加地址')?>
      </div>
    </div>
  </div>
</div>
<div id="container">
  <?php echo $html; ?>
</div>


<script language="javascript" type="text/javascript">
  var folder_name="<?php echo $folder_name?>";
  var controller_name ="<?php echo $controller_name?>";
  require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
    require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/list.js']);
  });
</script>