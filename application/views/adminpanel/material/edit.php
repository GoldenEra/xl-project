<?php defined('BASEPATH') or exit('No direct script access allowed.'); ?>

<form class="form-horizontal" role="form" id="validateform" name="validateform" method="post" enctype="multipart/form-data" action="<?php echo  base_url($folder_name.'/'.$controller_name.'/add')?>" >

<div class='panel panel-default'>
    <div class='panel-heading'>
        <i class='icon-edit icon-large'></i>
        <?php echo $is_edit?"修改":"新增"?>资料信息
        <div class='panel-tools'>

            <div class='btn-group'>
                <?php aci_ui_a($folder_name,'user','index','',' class="btn  btn-sm "','<span class="glyphicon glyphicon-arrow-left"></span> 返回')?>
            </div>
        </div>
    </div>
    <div class='panel-body'>
        <fieldset>
                <legend>资料的基本信息</legend>
                    
                    <div class="form-group material_name">
                        <label class="col-sm-2 control-label">资料名字</label>
                        <div class="col-sm-5">
                            <input type="text" name="material_name" value="<?php echo $material['name'] ? : '';?>" id="material_name"  class="form-control validate[required]"  placeholder="请输入名字" >
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label">请选择分类</label>
                        <div class="col-sm-5">
                            <select name="type_id" id="type_id" class=" form-control">
                                <option value="0">根分类</option>
                                <?php echo $select_categorys; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">请选择类型</label>
                        <div class="col-sm-5 type">
                            <select name="type" id="type" class="form-control">
                                <option value="text" <?php echo $material['type'] == 'text' ? 'selected' : ''; ?>>文本</option>
                                <option value="other" <?php echo $material['type'] != 'text' ? 'selected' : '';?>>视频/图片</option>
                            </select>
                        </div>
                    </div>
                    

                    <!-- 加载编辑器的容器 -->
                    <div class="form-group ueditor-content">
                        <label class="col-sm-2 control-label">请输入内容</label>
                        <div class="col-sm-10">
                            <script id="ueditor-container" name="ueditor-content" type="text/plain">
                                <?php if (isset($material['content']) && !empty($material['content'])): ?>
                                    <?php echo $material['content']; ?>
                                <?php endif ?>
                            </script>
                        </div>
                    </div>

                    
                    <div class="form-group material_file">
                        <label class="col-sm-2 control-label">请选择文件</label>
                        <div class="col-sm-10">
                          <input type="file" multiple="multiple" id="material_file_1" name="files[]">
                        </div>
                    </div>

                    <!-- 隐藏域里加入ID -->
                <?php if (isset($material['id'])): ?>

                <input type="hidden" name="material_id" value="<?php echo isset($material['id']) ? $material['id'] : ''  ?>"  class="form-control"/>
                
                <?php endif ?>


            </fieldset>

        <div class='form-actions'>
            <button type="submit" id="dosubmit" class="btn btn-primary ">保存</button>
        </div>
     </div>

</form>
<script language="javascript" type="text/javascript">

    var folder_name = "<?php echo $folder_name?>";
    var id = <?php echo isset($material['id']) ? $material['id'] : 0; ?>

    require(['<?php echo SITE_URL?>scripts/common.js'], function (common) {
        require(['<?php echo SITE_URL?>scripts/<?php echo $folder_name?>/<?php echo $controller_name?>/edit.js']);
    });
</script>
