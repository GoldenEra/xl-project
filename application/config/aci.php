<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['aci_status'] = array (
  'systemVersion' => '1.2.0',
  'installED' => true,
);
$config['aci_module'] = array (
  'welcome' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => 'welcome',
    'modulePath' => '',
    'moduleCaption' => '首页',
    'description' => '由autoCodeigniter 系统的模块',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => '',
    'system' => true,
    'coder' => '胡子锅',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => '',
        'controller' => 'welcome',
        'method' => '',
        'caption' => '欢迎界面',
      ),
    ),
  ),

  'adminpanel' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => 'user',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '后台管理中心',
    'description' => '由autoCodeigniter 系统的模块',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/user',
    'system' => true,
    'coder' => '胡子锅',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'index',
        'caption' => '管理中心-首页',
      ),
      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'login',
        'caption' => '管理中心-登录',
      ),
      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'logout',
        'caption' => '管理中心-注销',
      ),
      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'profile',
        'method' => 'change_pwd',
        'caption' => '管理中心-修改密码',
      ),
      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'login',
        'caption' => '管理中心-登录',
      ),
      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'go',
        'caption' => '管理中心-URL转向',
      ),
      6 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'manage',
        'method' => 'cache',
        'caption' => '管理中心-全局缓存',
      ),
    ),
  ),

  'user' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => 'user',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '用户 / 用户组管理',
    'description' => '由autoCodeigniter 系统的模块',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/user',
    'system' => true,
    'coder' => '胡子锅',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'index',
        'caption' => '用户管理-列表',
      ),
      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'check_username',
        'caption' => '用户管理-检测用户名',
      ),
      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'delete',
        'caption' => '用户管理-删除',
      ),
      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'lock',
        'caption' => '用户管理-锁定',
      ),
      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'edit',
        'caption' => '用户管理-编辑',
      ),
      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'add',
        'caption' => '用户管理-新增',
      ),
      6 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'user',
        'method' => 'upload',
        'caption' => '用户管理-上传图像',
      ),
      7 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'role',
        'method' => 'index',
        'caption' => '用户组管理-列表',
      ),
      8 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'role',
        'method' => 'setting',
        'caption' => '用户组管理-权限设置',
      ),
      9 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'role',
        'method' => 'add',
        'caption' => '用户组管理-新增',
      ),
      10 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'role',
        'method' => 'edit',
        'caption' => '用户组管理-编辑',
      ),
      11 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'role',
        'method' => 'delete_one',
        'caption' => '用户组管理-删除',
      ),
      12 =>
          array (
              'folder' => 'adminpanel',
              'controller' => 'user',
              'method' => 'user_window',
              'caption' => '用户-弹窗',
          ),
      13 =>
          array (
              'folder' => 'adminpanel',
              'controller' => 'role',
              'method' => 'group_window',
              'caption' => '用户组-弹窗',
          ),
    ),
  ),

  'moduleMenu' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => 'moduleMenu',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '菜单管理',
    'description' => '由autoCodeigniter 系统的模块',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/moduleMenu',
    'system' => true,
    'coder' => '胡子锅',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleMenu',
        'method' => 'index',
        'caption' => '菜单管理-列表',
      ),
      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleMenu',
        'method' => 'add',
        'caption' => '菜单管理-新增',
      ),
      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleMenu',
        'method' => 'edit',
        'caption' => '菜单管理-编辑',
      ),
      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleMenu',
        'method' => 'delete',
        'caption' => '菜单管理-删除',
      ),
      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleMenu',
        'method' => 'set_menu',
        'caption' => '菜单管理-设置菜单',
      ),
    ),
  ),

  'moduleManage' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => 'module',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '模块安装管理',
    'description' => '由autoCodeigniter 系统的模块',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/moduleManage',
    'system' => true,
    'coder' => '胡子锅',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleManage',
        'method' => 'index',
        'caption' => '模块管理',
      ),
      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'index',
        'caption' => '模块管理-开始',
      ),
      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'check',
        'caption' => '模块管理-检查',
      ),
      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'setup',
        'caption' => '模块管理-安装',
      ),
      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'uninstall',
        'caption' => '模块管理-卸载',
      ),
      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'reinstall',
        'caption' => '模块管理-重新安装',
      ),
      6 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'moduleInstall',
        'method' => 'delete',
        'caption' => '模块管理-删除',
      ),
    ),
  ),

  'address' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => '地址管理',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '地址管理',
    'description' => '用于增加、修改地址',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/address',
    'system' => false,
    'coder' => 'richard',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'address',
        'method' => 'index',
        'caption' => '地址列表',
      ),
      // 1 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'address',
      //   'method' => 'addPage',
      //   'caption' => '添加地址页面',
      // ),
      // 2 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'address',
      //   'method' => 'add',
      //   'caption' => '添加地址方法',
      // ),
      // 3 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'address',
      //   'method' => 'edit',
      //   'caption' => '编辑地址',
      // ),
      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'address',
        'method' => 'delete',
        'caption' => '删除地址',
      ),


      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'address',
        'method' => 'indexTree',
        'caption' => '树形结构',
      ),


    ),

  ),

  'camera' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => '摄像头管理',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '摄像头管理',
    'description' => '用于增加、修改摄像头信息',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/camera',
    'system' => false,
    'coder' => 'richard',
    'website' => 'http://',
    'moduleDetails' => 
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'camera',
        'method' => 'index',
        'caption' => '摄像头列表',
      ),
      // 1 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'camera',
      //   'method' => 'addPage',
      //   'caption' => '增加摄像头信息页面',
      // ),
      // 2 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'camera',
      //   'method' => 'add',
      //   'caption' => '增加摄像头信息',
      // ),
      // 3 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'camera',
      //   'method' => 'editPage',
      //   'caption' => '编辑摄像头信息',
      // ),
      // 4 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'camera',
      //   'method' => 'update',
      //   'caption' => '更新摄像头信息',
      // ),
      // 5 => 
      // array (
      //   'folder' => 'adminpanel',
      //   'controller' => 'camera',
      //   'method' => 'delete',
      //   'caption' => '删除摄像头',
      // ),
      6 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'camera',
        'method' => 'bindGroup',
        'caption' => '绑定用户组',
      ),

      7 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'camera',
        'method' => 'cameraMap',
        'caption' => '摄像头地图',
      ),

      8 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'camera',
        'method' => 'messageMap',
        'caption' => '报警信息地图',
      ),

      9 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'roleCamera',
        'method' => 'setting',
        'caption' => '用户组管理-摄像头权限设置',
      ),
    ),
  ),

  'message' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => '消息管理',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '消息管理',
    'description' => '用于增加、修改消息',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/message',
    'system' => false,
    'coder' => 'richard',
    'website' => 'http://',
    'moduleDetails' => 
    
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'index',
        'caption' => '首页',
      ),

      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'detail',
        'caption' => '查看详情',
      ),

      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'toLeader',
        'caption' => '上报上级',
      ),

      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'autoToLeader',
        'caption' => '信息自动上报',
      ),

      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'detail',
        'caption' => '报警详情',
      ),

      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'messageMap',
        'caption' => '信息处理地图',
      ),

      6 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'message',
        'method' => 'updateMessageStatus',
        'caption' => '信息处理',
      ),

    ),
  ),

  'materialType' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => '报警分类',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '报警分类管理',
    'description' => '',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/materialType',
    'system' => false,
    'coder' => 'richard',
    'website' => 'http://',
    'moduleDetails' => 
    
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'materialType',
        'method' => 'index',
        'caption' => '类型管理页',
      ),

      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'materialType',
        'method' => 'add',
        'caption' => '增加分类',
      ),

      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'materialType',
        'method' => 'edit',
        'caption' => '修改分类',
      ),

      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'materialType',
        'method' => 'delete',
        'caption' => '删除分类',
      ),

      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'materialType',
        'method' => 'addPage',
        'caption' => '添加地址页面',
      ),
      
    ),
  ),

  'material' => 
  array (
    'version' => '1',
    'charset' => 'utf-8',
    'lastUpdate' => '2015-10-09 20:10:10',
    'moduleName' => '报警分类',
    'modulePath' => 'adminpanel',
    'moduleCaption' => '报警分类管理',
    'description' => '',
    'fileList' => NULL,
    'works' => true,
    'moduleUrl' => 'adminpanel/material',
    'system' => false,
    'coder' => 'richard',
    'website' => 'http://',
    'moduleDetails' => 
    
    array (
      0 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'index',
        'caption' => '资料管理页',
      ),

      1 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'add',
        'caption' => '增加资料',
      ),

      2 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'detail',
        'caption' => '资料详情',
      ),

      3 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'edit',
        'caption' => '修改资料',
      ),

      4 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'delete',
        'caption' => '删除资料',
      ),

      5 => 
      array (
        'folder' => 'adminpanel',
        'controller' => 'material',
        'method' => 'addPage',
        'caption' => '添加地址页面',
      ),
    ),
  ),


);

/* End of file aci.php */
/* Location: ./application/config/aci.php */
