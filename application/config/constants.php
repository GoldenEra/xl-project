<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');




/*
|--------------------------------------------------------------------------
| 短信发送地址
|--------------------------------------------------------------------------
*/
define('SMS_BASE_URL', "http://192.168.1.1:9002/sms/msg=");

/*
|--------------------------------------------------------------------------
| 海康服务器配置
|--------------------------------------------------------------------------
|HAIKANG_SERVER_IP 		: 	海康服务器IP
|HAIKANG_SERVER_PORT 	: 	端口
|HAIKANG_SERVER_IP 		: 	登录名
|HAIKANG_SERVER_IP 		: 	密码
*/
define('HAIKANG_SERVER_IP', 		'223.85.88.126');
define('HAIKANG_SERVER_PORT', 		81);
define('HAIKANG_SERVER_USERNAME', 	'admin');
define('HAIKANG_SERVER_PASSWORD', 	'Ab123456');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('HTTP_REFERER', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
define('SITE_URL', '/');#初始安装，请在这里修改,为实际目录
define('SKIN_PATH', SITE_URL.'css/');
define('SYS_STYLE',  'default');
define('EXT',  '.php');


define('UPLOAD_FOLDER_NAME', 'uploadfile');
define('BASE_CSS_PATH',  SITE_URL.'css/');
define('BASE_JS_PATH', SITE_URL.'scripts/');
define('IMG_PATH', SITE_URL.'images/');
define('UPLOAD_URL', SITE_URL.UPLOAD_FOLDER_NAME.'/');

define('UPLOAD_TEMP_URL', SITE_URL.UPLOAD_FOLDER_NAME.'/temp/');
define('UPLOAD_PATH',FCPATH.UPLOAD_FOLDER_NAME.'/');
define('UPLOAD_TEMP_PATH',''.FCPATH.UPLOAD_FOLDER_NAME.'/temp/');
define('INTALL_UPLOAD_TEMP_PATH',''.FCPATH.UPLOAD_FOLDER_NAME.'/temp/install');

define('LOG_PATH',FCPATH.'application/logs/');
define('SYS_TIME', time());

define('SITE_NAME','雪亮工程');
define('WEBSITE_BASE_NAME','雪亮工程');


define('SUPERADMIN_GROUP_ID',  1);
define('REGISTER_GROUP_ID',  3);
define('DEMO_STATUS',  FALSE);//演示版本状态，有权限控制
define('SETUP_BACKUP_OVERWRITE_FILES', FALSE);#是否备份存在的模块文件


define('ADMIN_URL_PATH',  SITE_URL.'adminpanel/');
define('ADMIN_CSS_PATH',  SITE_URL.'css/adminpanel/');
define('ADMIN_IMG_PATH', SITE_URL.'images/adminpanel/');

// 自动报警间隔为2小时
define('CALL_POlICE_INTERVAL', 60 * 60 * 2);
define('PAGE_SIZE', 20);
define('MAX_PAGE_SIZE', 9999999);
// 最大上传，1G
define('UPLOAD_SIZE', 10240000);

// AES加密key
define('AES_KEY', 'THISISAXLPROJECT');

/**
 * url中的accountSid。如果接口验证级别是主账户则传网站“个人中心”页面的“账户ID”，
 */
define('SMS_ACCOUNT_SID', "0887eeb4b69d4932a66c2d89bc7de698"); // 主账)户
define('SMS_AUTH_TOKEN', "48d52e86e3dd4cf5bbead26b33272ecb");

/*)*
 * 请求的内容类型，application/x-www-form-urlencoded
 */
define('SMS_CONTENT_TYPE', "application/x-www-form-urlencoded");

/**
 * 期望服务器响应的内容类型，可以是application/json或application/xml
 */
define('SMS_ACCEPT', "application/json");
// 发送短信类型，详见http://www.miaodiyun.com/doc/https_sms.html,这里是发送验证码短信
define('SMS_TYPE', 'industrySMS/sendSMS/');

define('SMS_CONTEND', "【雪亮工程】有新的报警信息，请在《雪亮工程》软件中的“报警信息”功能下查看报警详情");
//define('SMS_CONTEND', "雪亮工程");

// 高德地图根据使用的服务不同，要申请的API 也不同，这里是根据经纬度获取地址的
define('GAODE_KEY_ADDR', 'b4c85091814aafaccdc4501ed6eebaad');




/* End of file constants.php */
/* Location: ./application/config/constants.php */
