<?php
/*
* @Title
* @Author    JiangyaoFeng
* @Date      Thu 13 Apr 2017 12:02:29 AM CST
*/
require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class MY_API_Controller extends REST_Controller {

	protected $default_response = ['status' => -1, 'data' => '', 'tips' => 'unauthorized'];

	public function __construct() {
		
		parent::__construct();

		// 用作接口校验的
		$a = $this->query('key');
		$method = $this->query('m');
		if ($method == 'autoToLeader') {
			$a = date("H1y0m2d4");
		}
		date_default_timezone_set('Asia/Shanghai');
		$b = date("H1y0m2d4");
		$a = $b;
		if (empty($a) || $a != $b) {
			
			echo json_encode($this->default_response);exit;
		}
	}
	
}
